package com.ecommerce.shoppingservice.util.aspects;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@Aspect
public class LoggerDecorator {

 private Log log = LogFactory.getLog(getClass());
 
    @Around("execution(* com.ecommerce.shoppingservice.service.ShoppingService.*(..))")
    public Object log(ProceedingJoinPoint joinPoint) throws Throwable {
    LocalDateTime start = LocalDateTime.now();

    Object toReturn = null;
    Throwable exceptionCaught = null;
    try {
        toReturn = joinPoint.proceed();
    }
    catch (Throwable t) {
        exceptionCaught = t;
    }
    LocalDateTime stop = LocalDateTime.now();

    log.info("Service execution start: " + start.toString() + " | Service execution end: " + stop.toString() + " | Total execution time: "
                + stop.minusNanos(start.getNano()).getNano());

    
    if (null != exceptionCaught){
        log.info("Caught Exception: " + exceptionCaught.getMessage() + " while doing: " + joinPoint.getSignature().getName());
        throw exceptionCaught;
    }
    
    return toReturn;
    }
}