package com.ecommerce.shoppingservice.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.security.Principal;
import java.util.UUID;

import javax.annotation.security.RolesAllowed;
import org.springframework.security.access.prepost.PreAuthorize;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import com.ecommerce.shoppingservice.domain.Category;
import com.ecommerce.shoppingservice.domain.Product;
import com.ecommerce.shoppingservice.mapper.CategoryMapper;
import com.ecommerce.shoppingservice.mapper.ProductMapper;
import com.ecommerce.shoppingservice.model.CategoryDto;
import com.ecommerce.shoppingservice.model.ProductDto;
import com.ecommerce.shoppingservice.service.ShoppingService;

/**
 * Restful api for shopping service.
 * 
 * @author Onur Keskin
 */
@RestController
@RequestMapping(value = "rest/shopping/v1", produces = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
@Slf4j
public class ShoppingServiceController {
    private ShoppingService shoppingService;

    /**
     * Endpoint that reads categories from service and responds with serialized
     * data.
     * 
     * @return Categories currently available.
     */
    @GetMapping("/category")
    @PreAuthorize("hasAuthority('SCOPE_shopping') and hasRole('ROLE_user')")
    @ResponseBody
    public ResponseEntity<Iterable<Category>> getCategories() {
        return new ResponseEntity<>(shoppingService.getAllCategories(), HttpStatus.OK);
    }

    /**
     * Endpoint that requests a new category to be created from service.
     * 
     * @return Category that is created. Or
     */
    @PostMapping("/category")
    @PreAuthorize("hasAuthority('SCOPE_shopping') and hasRole('ROLE_admin')")
    @ResponseBody
    public ResponseEntity<Category> createCategory(@RequestBody CategoryDto category) {
        if(category.getName() == null){
            return ResponseEntity.badRequest().build();
        }

        Category toAdd = CategoryMapper.INSTANCE.CategoryDtoToCategory(category);

        return new ResponseEntity<>(shoppingService.createCategory(toAdd), HttpStatus.OK);
    }

    /**
     * Endpoint that gets all products from service and.
     * 
     * @return Products .
     */
    @GetMapping("/products")
    @PreAuthorize("hasAuthority('SCOPE_shopping') and hasRole('ROLE_user')")
    @ResponseBody
    public ResponseEntity<Iterable<Product>> getProducts() {
        return new ResponseEntity<>(shoppingService.getAllProducts(), HttpStatus.OK);
    }

    /**
     * Endpoint that gets all products from service.
     * 
     * @return Categories currently available.
     */
    @GetMapping("/products/search")
    @PreAuthorize("hasAuthority('SCOPE_shopping') and hasRole('ROLE_user')")
    @ResponseBody
    public ResponseEntity<Iterable<Product>> getProducts(@RequestParam("category") UUID category_id,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "num", required = false) Integer num) {
        if (num == null) {
            num = 20;
        }
        if (page == null) {
            page = 0;
        }
        return new ResponseEntity<>(
                shoppingService.getProductsByCategoryPaginated( category_id, "product_id", (int) page, (int) num),
                HttpStatus.OK);
    }

    @PostMapping("/products")
    @PreAuthorize("hasAuthority('SCOPE_shopping') and hasRole('ROLE_admin')")
    @ResponseBody
    public ResponseEntity<Product> createProduct(Principal principal, @RequestBody ProductDto productDto) {
        Product toAdd = ProductMapper.INSTANCE.ProductDtoToProduct(productDto);
        toAdd.setUserId(UUID.fromString(principal.getName()));
        
        log.info("Adding product: " + toAdd.toString());
        return new ResponseEntity<>(shoppingService.createProduct(toAdd), HttpStatus.OK);
    }

    @GetMapping("/products/{product_id}")
    @PreAuthorize("hasAuthority('SCOPE_shopping') and hasRole('ROLE_user')")
    @ResponseBody
    public ResponseEntity<Product> getProduct(@PathVariable("product_id") UUID product_id) {
        try {
            return new ResponseEntity<Product>(shoppingService.getProduct(product_id).get(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/products/{product_id}")
    @PreAuthorize("hasAuthority('SCOPE_shopping') and hasRole('ROLE_admin')")
    @ResponseBody
    public ResponseEntity<Product> updateProduct(Principal principal, @RequestBody ProductDto productDto,
            @PathVariable("product_id") UUID product_id) {
        Product toUpdate = ProductMapper.INSTANCE.ProductDtoToProduct(productDto);
        toUpdate.setId(product_id);
        try {
            return new ResponseEntity<Product>(shoppingService.updateProduct(toUpdate), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/products/{product_id}")
    @PreAuthorize("hasAuthority('SCOPE_shopping') and hasRole('ROLE_admin')")
    @ResponseBody
    public ResponseEntity<Product> deleteProduct(Principal principal, @PathVariable("product_id") UUID product_id) {
        try {
            return new ResponseEntity<Product>(shoppingService.deleteProduct(product_id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
}
