package com.ecommerce.shoppingservice.domain;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.JoinColumn;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.FetchType;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Category Entity
 * 
 * @author Onur Keskin
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Category")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "categoryId")
    private UUID id;

    @Column(unique = true)
    private String name;

    @Override
    public int hashCode() {
        return java.util.Objects.hashCode(name);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        return ((Category) o).name.equals(this.name);
    }

    @JsonIgnore
    @ManyToMany(cascade = CascadeType.ALL)
    // (cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinTable(name = "ProductCategory", 
                joinColumns = @JoinColumn(name = "categoryId", nullable = false), 
                inverseJoinColumns = @JoinColumn(name = "productId", nullable = false))
    private Set<Product> products = new HashSet<>();

    public void addProduct(Product p){
        products.add(p);
    }
}
