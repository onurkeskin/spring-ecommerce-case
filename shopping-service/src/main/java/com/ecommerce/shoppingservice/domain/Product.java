package com.ecommerce.shoppingservice.domain;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;

/**
 * Product Entity.
 * 
 * @author Onur Keskin
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "productId")
    private UUID id;

    private UUID userId;

    private String name;
    private Integer count;
    private Double price;

    @Override
    public int hashCode() {
        return java.util.Objects.hashCode(name + "" + id);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        return ((Product) o).id.equals(this.id);
    }

    @ManyToMany(mappedBy = "products")
    private Set<Category> categories = new HashSet<Category>();
}
