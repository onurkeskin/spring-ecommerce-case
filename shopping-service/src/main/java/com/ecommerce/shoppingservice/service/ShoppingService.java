package com.ecommerce.shoppingservice.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.ecommerce.shoppingservice.domain.Category;
import com.ecommerce.shoppingservice.domain.Product;
import com.ecommerce.shoppingservice.model.exceptions.ProductNotFoundException;

public interface ShoppingService {

    public Category createCategory(Category category);
    List<Category> getAllCategories();

    public Iterable<Product> getAllProducts();
    List<Product> getProductsByCategoryPaginated(UUID categoryId, String sortedOn, int pageNumber, int numberOfProducts);
    public Product createProduct(Product product);
    public Optional<Product> getProduct(UUID id);
	Product updateProduct(Product toUpdate) throws ProductNotFoundException;
	public Product deleteProduct(UUID product_id) throws ProductNotFoundException;
}
