package com.ecommerce.shoppingservice.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

import com.ecommerce.shoppingservice.dao.CategoryRepository;
import com.ecommerce.shoppingservice.dao.ProductRepository;

import com.ecommerce.shoppingservice.domain.Category;
import com.ecommerce.shoppingservice.domain.Product;
import com.ecommerce.shoppingservice.model.exceptions.ProductNotFoundException;
import com.ecommerce.shoppingservice.service.ShoppingService;

/**
 * Service implementation for shopping.
 * 
 * @author Onur Keskin
 */
@Service
@RequiredArgsConstructor
public class ShoppingServiceImpl implements ShoppingService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;

    /**
     * Queries the repository for all categories
     * 
     * @return a List of all available categories
     */
    public List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }

    /**
     * Adds a new category type to the repository
     * 
     * @param category category to be added
     * @return the added category if success or null if fail
     */
    public Category createCategory(Category category) {
        try {
            return categoryRepository.save(category);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Queries the repository for all Products.
     * 
     * @return A list of all available products.
     */
    public Iterable<Product> getAllProducts() {
        return productRepository.findAll();
    }

    /**
     * Queries the repository for products from a category.
     * 
     * @param categoryId       id of category to be queried.
     * @param sortedOn         sorts the query result desc for the parameter.
     * @param pageNumber       for pagination
     * @param numberOfProducts for pagination
     * @return A list of all available products.
     */
    public List<Product> getProductsByCategoryPaginated(UUID categoryId, String sortedOn, int pageNumber,
            int numberOfProducts) {
        // TODO: pagination not works requires native query support in repository.

        // Page<Product> page = productRepository
        // .findByCategories_Id(categoryId, PageRequest.of(pageNumber, numberOfProducts,
        // Sort.by(Sort.Direction.ASC, sortedOn)));

        List<Product> page = productRepository.findByCategories_Id(categoryId);

        return page;
    }

    /**
     * Queries repository and returns a product.
     * 
     * @param id of the product to query.
     * @return Optional<Product>
     */
    public Optional<Product> getProduct(UUID id) {
        return productRepository.findById(id);
    }

    /**
     * Adds a new product
     * 
     * @param product
     * @return product
     */
    public Product createProduct(Product product) {
        // if (productRepository.findById(product.getId()).isPresent()) {
        //     return null;
        // }

        productRepository.save(product);
        
        for (Category cat : product.getCategories()) {
            Optional<Category> inRepo = categoryRepository.findById(cat.getId());
            if (inRepo.isPresent()) {
                inRepo.get().addProduct(product);
                categoryRepository.save(inRepo.get());
            }
        }


        return product;
    }

    /**
     * Updates a product.
     * 
     * @param product to be added or incremented
     * @return product that is added or incremented.
     */
    public Product updateProduct(Product toUpdate) throws ProductNotFoundException {
        Optional<Product> existing = productRepository.findById(toUpdate.getId());
        if (!existing.isPresent()) {
            throw new ProductNotFoundException();
        }

        return productRepository.save(toUpdate);
    }

    public Product deleteProduct(UUID product_id) throws ProductNotFoundException {
        Optional<Product> existing = productRepository.findById(product_id);
        if (!existing.isPresent()) {
            throw new ProductNotFoundException();
        }

        productRepository.delete(existing.get());

        return existing.get();
    }

}
