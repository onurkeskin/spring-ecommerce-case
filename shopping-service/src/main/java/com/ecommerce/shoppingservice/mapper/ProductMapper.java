package com.ecommerce.shoppingservice.mapper;

import com.ecommerce.shoppingservice.domain.Product;
import com.ecommerce.shoppingservice.model.ProductDto;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * Mapper class for converting Request/Response models to DTOs or vice versa.
 * 
 * @author Onur Keskin
 */
@Mapper(uses = CategoryMapper.class)
public interface ProductMapper {
    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

    Product ProductDtoToProduct(ProductDto ProductDto);
    ProductDto ProductToProductDto(Product ProductDto);
}
