package com.ecommerce.shoppingservice.mapper;

import com.ecommerce.shoppingservice.domain.Category;
import com.ecommerce.shoppingservice.model.CategoryDto;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * Mapper class for converting Request/Response models to DTOs or vice versa.
 * 
 * @author Onur Keskin
 */
@Mapper
public interface CategoryMapper {
    CategoryMapper INSTANCE = Mappers.getMapper(CategoryMapper.class);

    Category CategoryDtoToCategory(CategoryDto CategoryDto);
    CategoryDto CategoryToCategoryDto(Category CategoryDto);
}
