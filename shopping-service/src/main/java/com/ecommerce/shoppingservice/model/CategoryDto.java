package com.ecommerce.shoppingservice.model;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Dto for Category
 * 
 * @author Onur Keskin
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryDto {
    public UUID id;
    public String name;
}