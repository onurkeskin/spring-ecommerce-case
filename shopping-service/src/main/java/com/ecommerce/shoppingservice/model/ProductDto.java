package com.ecommerce.shoppingservice.model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * Dto for Product
 * 
 * @author Onur Keskin
 */
@Data
@AllArgsConstructor
public class ProductDto {
    public String name;
    public UUID userId;
    public Integer count;
    public Double price;

    public List<CategoryDto> categories;

    public ProductDto(){
        categories = new ArrayList<>();
    }

    public void addCategory(CategoryDto categoryDto) {
        if (categories == null) {
            categories = new ArrayList<>();
        }

        categories.add(categoryDto);
    }
}