package com.ecommerce.shoppingservice.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import com.ecommerce.shoppingservice.domain.*;


/**
 * Product Repository
 * TODO: Clean and redo mess
 * @author Onur Keskin
 */
public interface ProductRepository extends JpaRepository<Product, UUID> {
    List<Product> findByName(String name);
    List<Product> findByUserId(UUID userId);
    // @Query(value = "SELECT * FROM Product LEFT JOIN Product_category on Product.product_id = Product_category.product_id LEFT JOIN Category on Product_category.category_id = Category.category_id where Product_category.category_id = :id",
    //     countQuery = "SELECT count(*) FROM Product LEFT JOIN Product_category on Product.product_id = Product_category.product_id LEFT JOIN Category on Product_category.category_id = Category.category_id where Product_category.category_id = :id",
    //     nativeQuery = true
    // )
    // Page<Product> findByCategories_Id(@Param("id")Long category, Pageable pageable);
    // Page<Product> findByCategories_Id(Long id, Pageable pageable);

    // @Query("select p FROM Product p join fetch p.categories c where c.id = :id")
    List<Product> findByCategories_Id(@Param("id") UUID category);
}