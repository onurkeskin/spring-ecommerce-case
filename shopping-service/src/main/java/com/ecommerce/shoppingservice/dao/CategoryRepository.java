package com.ecommerce.shoppingservice.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

import com.ecommerce.shoppingservice.domain.*;

/**
 * Category Repository
 * TODO: Clean and redo mess
 * @author Onur Keskin
 */
public interface CategoryRepository extends JpaRepository<Category, UUID> {
    Page<Category> findAll(Pageable pageable);
    Optional<Category> findByName(String name);
}