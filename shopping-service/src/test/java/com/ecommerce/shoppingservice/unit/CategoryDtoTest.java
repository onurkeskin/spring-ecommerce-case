package com.ecommerce.shoppingservice.unit;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.containsString;

import com.ecommerce.shoppingservice.model.CategoryDto;

import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
public class CategoryDtoTest {

    @Test
    public void CategoryDtoNoArgConsturctibleToNullFieldsTest() throws Exception {
        assertDoesNotThrow(() -> {
            CategoryDto dto = new CategoryDto();
            assertThat(dto.getId()).isNull();
            assertThat(dto.getName()).isNull();
        });
    }

    @Test
    public void CategoryDtoAllArgsConsturctibleTest() throws Exception {
        assertDoesNotThrow(() -> {
            UUID testId = UUID.randomUUID();
            String testName = "testName";

            CategoryDto dto = new CategoryDto(testId, testName);
            assertThat(dto.getName()).isEqualTo(testName);
            assertThat(dto.getId()).isEqualTo(testId);
        });
    }

    @Test
    public void GetUserIdTest() throws Exception {
        assertDoesNotThrow(() -> {
            UUID testId = UUID.randomUUID();
            String testName = "testName";

            CategoryDto dto = new CategoryDto(testId, testName);
            assertThat(dto.getId()).isEqualTo(testId);
        });
    }

    @Test
    public void GetNameTest() throws Exception {
        assertDoesNotThrow(() -> {
            UUID testId = UUID.randomUUID();
            String testName = "testName";

            CategoryDto dto = new CategoryDto(testId, testName);
            assertThat(dto.getName()).isEqualTo(testName);
        });
    }

    @Test
    public void SetIdUpdatesIdTest() throws Exception {
        assertDoesNotThrow(() -> {
            UUID testId = UUID.randomUUID();
            UUID testId2 = UUID.randomUUID();
            String testName = "testName";

            CategoryDto dto = new CategoryDto(testId, testName);
            assertThat(dto.getId()).isEqualTo(testId);
            dto.setId(testId2);
            assertThat(dto.getId()).isEqualTo(testId2);
        });
    }

    @Test
    public void SetNameUpdatesNameTest() throws Exception {
        assertDoesNotThrow(() -> {
            UUID testId = UUID.randomUUID();
            String testName = "testName";
            String testName2 = "testName2";

            CategoryDto dto = new CategoryDto(testId, testName);
            assertThat(dto.getName()).isEqualTo(testName);
            dto.setName(testName2);
            assertThat(dto.getName()).isEqualTo(testName2);
        });
    }

    @Test
    public void CategoryDtoReferenceEqualityTest() throws Exception {
        assertDoesNotThrow(() -> {
            UUID testId = UUID.randomUUID();
            String testName = "testName";
            String testName2 = "testName2";

            CategoryDto dto = new CategoryDto(testId, testName);
            assertThat(dto.equals(dto)).isTrue();
        });
    }

    @Test
    public void CategoryDtoDeepEqualityTest() throws Exception {
        assertDoesNotThrow(() -> {
            UUID testId = UUID.randomUUID();
            String testName = "testName";
            String testName2 = "testName2";

            CategoryDto dto = new CategoryDto(testId, testName);
            CategoryDto dto2 = new CategoryDto(testId, testName);
            CategoryDto dto3 = new CategoryDto(testId, testName2);
            assertThat(dto.equals(dto2)).isTrue();
            assertThat(dto.equals(dto3)).isFalse();
        });
    }

    @Test
    public void CategoryDtoHashCodeSameRefMustSameResultTest() throws Exception {
        assertDoesNotThrow(() -> {
            UUID testId = UUID.randomUUID();
            String testName = "testName";
            String testName2 = "testName2";

            CategoryDto dto = new CategoryDto(testId, testName);
            assertThat(dto.hashCode()).isEqualTo(dto.hashCode());
        });
    }

    @Test
    public void CategoryDtoHashCodeDifferentRefMustSameFieldsMustBeSameTest() throws Exception {
        assertDoesNotThrow(() -> {
            UUID testId = UUID.randomUUID();
            String testName = "testName";

            CategoryDto dto = new CategoryDto(testId, testName);
            CategoryDto dto2 = new CategoryDto(testId, testName);
            assertThat(dto.hashCode()).isEqualTo(dto2.hashCode());
        });
    }

    @Test
    public void ToStringTest() throws Exception {
        assertDoesNotThrow(() -> {
            UUID testId = UUID.randomUUID();
            String testName = "testName";

            CategoryDto dto = new CategoryDto(testId, testName);

            assertThat(dto.toString(), CoreMatchers.allOf(containsString("testName"), containsString("" + testId)));
        });
    }
}
