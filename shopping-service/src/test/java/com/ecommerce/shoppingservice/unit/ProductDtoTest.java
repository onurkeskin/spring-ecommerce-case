package com.ecommerce.shoppingservice.unit;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.containsString;

import com.ecommerce.shoppingservice.model.ProductDto;
import com.ecommerce.shoppingservice.model.CategoryDto;

import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
public class ProductDtoTest {

    @Test
    public void ProductDtoNoArgConsturctibleToNullFieldsTest() throws Exception {
        assertDoesNotThrow(() -> {
            ProductDto dto = new ProductDto();
            assertThat(dto.getName()).isNull();
            assertThat(dto.getUserId()).isNull();
            assertThat(dto.getCount()).isNull();
            assertThat(dto.getPrice()).isNull();
            assertThat(dto.getCategories()).isNotNull();
        });
    }

    @Test
    public void ProductDtoAllArgsConsturctibleTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testName = "testName";
            UUID testUserId = UUID.randomUUID();
            Integer testCount = 1;
            Double testPrice = 2d;
            List<CategoryDto> testCategories = new ArrayList<>();

            ProductDto dto = new ProductDto(testName, testUserId, testCount, testPrice, testCategories);
            assertThat(dto.getName()).isEqualTo(testName);
            assertThat(dto.getUserId()).isEqualTo(testUserId);
            assertThat(dto.getCount()).isEqualTo(testCount);
            assertThat(dto.getPrice()).isEqualTo(testPrice);
            assertThat(dto.getCategories()).isEqualTo(testCategories);
        });
    }

    @Test
    public void GetNameTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testName = "testName";
            UUID testUserId = UUID.randomUUID();
            Integer testCount = 1;
            Double testPrice = 2d;
            List<CategoryDto> testCategories = new ArrayList<>();

            ProductDto dto = new ProductDto(testName, testUserId, testCount, testPrice, testCategories);
            assertThat(dto.getName()).isEqualTo(testName);
        });
    }

    @Test
    public void GetUserIdTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testName = "testName";
            UUID testUserId = UUID.randomUUID();
            Integer testCount = 1;
            Double testPrice = 2d;
            List<CategoryDto> testCategories = new ArrayList<>();

            ProductDto dto = new ProductDto(testName, testUserId, testCount, testPrice, testCategories);
            assertThat(dto.getUserId()).isEqualTo(testUserId);
        });
    }

    @Test
    public void GetCountTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testName = "testName";
            UUID testUserId = UUID.randomUUID();
            Integer testCount = 1;
            Double testPrice = 2d;
            List<CategoryDto> testCategories = new ArrayList<>();

            ProductDto dto = new ProductDto(testName, testUserId, testCount, testPrice, testCategories);
            assertThat(dto.getCount()).isEqualTo(testCount);
        });
    }

    @Test
    public void GetPriceTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testName = "testName";
            UUID testUserId = UUID.randomUUID();
            Integer testCount = 1;
            Double testPrice = 2d;
            List<CategoryDto> testCategories = new ArrayList<>();

            ProductDto dto = new ProductDto(testName, testUserId, testCount, testPrice, testCategories);
            assertThat(dto.getPrice()).isEqualTo(testPrice);
        });
    }

    @Test
    public void GetCategoriesTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testName = "testName";
            UUID testUserId = UUID.randomUUID();
            Integer testCount = 1;
            Double testPrice = 2d;
            List<CategoryDto> testCategories = new ArrayList<>();

            ProductDto dto = new ProductDto(testName, testUserId, testCount, testPrice, testCategories);
            assertThat(dto.getCategories()).isEqualTo(testCategories);
        });
    }

    @Test
    public void SetNameUpdatesNameTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testName = "testName";
            String testName2 = "testName2";
            UUID testUserId = UUID.randomUUID();
            Integer testCount = 1;
            Double testPrice = 2d;
            List<CategoryDto> testCategories = new ArrayList<>();

            ProductDto dto = new ProductDto(testName, testUserId, testCount, testPrice, testCategories);
            assertThat(dto.getName()).isEqualTo(testName);
            dto.setName(testName2);
            assertThat(dto.getName()).isEqualTo(testName2);
        });
    }

    @Test
    public void SetUserIdUpdatesUserIdTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testName = "testName";
            UUID testUserId = UUID.randomUUID();
            UUID testUserId2 = UUID.randomUUID();
            Integer testCount = 1;
            Double testPrice = 2d;
            List<CategoryDto> testCategories = new ArrayList<>();

            ProductDto dto = new ProductDto(testName, testUserId, testCount, testPrice, testCategories);
            assertThat(dto.getUserId()).isEqualTo(testUserId);
            dto.setUserId(testUserId2);
            assertThat(dto.getUserId()).isEqualTo(testUserId2);
        });
    }

    @Test
    public void SetCountUpdatesCountTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testName = "testName";
            UUID testUserId = UUID.randomUUID();
            Integer testCount = 1;
            Integer testCount2 = 2;
            Double testPrice = 2d;
            List<CategoryDto> testCategories = new ArrayList<>();

            ProductDto dto = new ProductDto(testName, testUserId, testCount, testPrice, testCategories);
            assertThat(dto.getCount()).isEqualTo(testCount);
            dto.setCount(testCount2);
            assertThat(dto.getCount()).isEqualTo(testCount2);
        });
    }

    @Test
    public void SetPriceUpdatesPriceTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testName = "testName";
            UUID testUserId = UUID.randomUUID();
            Integer testCount = 1;
            Double testPrice = 2d;
            Double testPrice2 = 3d;
            List<CategoryDto> testCategories = new ArrayList<>();

            ProductDto dto = new ProductDto(testName, testUserId, testCount, testPrice, testCategories);
            assertThat(dto.getPrice()).isEqualTo(testPrice);
            dto.setPrice(testPrice2);
            assertThat(dto.getPrice()).isEqualTo(testPrice2);
        });
    }

    @Test
    public void SetCategoriesUpdatesCategoriesTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testName = "testName";
            UUID testUserId = UUID.randomUUID();
            Integer testCount = 1;
            Double testPrice = 2d;
            List<CategoryDto> testCategories = new ArrayList<>();
            List<CategoryDto> testCategories2 = new ArrayList<>();

            ProductDto dto = new ProductDto(testName, testUserId, testCount, testPrice, testCategories);
            assertThat(dto.getCategories()).isEqualTo(testCategories);
            dto.setCategories(testCategories2);
            assertThat(dto.getCategories()).isEqualTo(testCategories2);
        });
    }

    @Test
    public void AddCategoryUpdatesCategoriesTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testName = "testName";
            UUID testUserId = UUID.randomUUID();
            Integer testCount = 1;
            Double testPrice = 2d;
            List<CategoryDto> testCategories = new ArrayList<>();
            CategoryDto cat1 = new CategoryDto();
            CategoryDto cat2 = new CategoryDto();
            testCategories.add(cat1);

            ProductDto dto = new ProductDto(testName, testUserId, testCount, testPrice, testCategories);
            assertThat(dto.getCategories()).isEqualTo(testCategories);
            assertThat(dto.getCategories()).contains(cat1);
            dto.addCategory(cat2);
            assertThat(dto.getCategories()).contains(cat1, cat2);
        });
    }

    @Test
    public void ProductDtoReferenceEqualityTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testName = "testName";
            UUID testUserId = UUID.randomUUID();
            Integer testCount = 1;
            Double testPrice = 2d;
            List<CategoryDto> testCategories = new ArrayList<>();

            ProductDto dto = new ProductDto(testName, testUserId, testCount, testPrice, testCategories);
            assertThat(dto.equals(dto)).isTrue();
        });
    }

    @Test
    public void ProductDtoDeepEqualityTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testName = "testName";
            UUID testUserId = UUID.randomUUID();
            Integer testCount = 1;
            Double testPrice = 2d;
            Double testPrice2 = 3d;
            List<CategoryDto> testCategories = new ArrayList<>();

            ProductDto dto = new ProductDto(testName, testUserId, testCount, testPrice, testCategories);
            ProductDto dto2 = new ProductDto(testName, testUserId, testCount, testPrice, testCategories);
            ProductDto dto3 = new ProductDto(testName, testUserId, testCount, testPrice2, testCategories);
            assertThat(dto.equals(dto2)).isTrue();
            assertThat(dto.equals(dto3)).isFalse();
        });
    }

    @Test
    public void ProductDtoHashCodeSameRefMustSameResultTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testName = "testName";
            UUID testUserId = UUID.randomUUID();
            Integer testCount = 1;
            Double testPrice = 2d;
            List<CategoryDto> testCategories = new ArrayList<>();

            ProductDto dto = new ProductDto(testName, testUserId, testCount, testPrice, testCategories);
            assertThat(dto.hashCode()).isEqualTo(dto.hashCode());
        });
    }

    @Test
    public void ProductDtoHashCodeDifferentRefMustSameFieldsMustBeSameTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testName = "testName";
            UUID testUserId = UUID.randomUUID();
            Integer testCount = 1;
            Double testPrice = 2d;
            List<CategoryDto> testCategories = new ArrayList<>();

            ProductDto dto = new ProductDto(testName, testUserId, testCount, testPrice, testCategories);
            ProductDto dto2 = new ProductDto(testName, testUserId, testCount, testPrice, testCategories);
            assertThat(dto.hashCode()).isEqualTo(dto2.hashCode());
        });
    }

    @Test
    public void ToStringTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testName = "testName";
            UUID testUserId = UUID.randomUUID();
            Integer testCount = 1;
            Double testPrice = 2d;
            List<CategoryDto> testCategories = new ArrayList<>();

            ProductDto dto = new ProductDto(testName, testUserId, testCount, testPrice, testCategories);

            assertThat(dto.toString(), CoreMatchers.allOf(containsString("testName"), containsString("" + testUserId),
                    containsString("" + testCount), containsString("" + testPrice)));
        });
    }
}
