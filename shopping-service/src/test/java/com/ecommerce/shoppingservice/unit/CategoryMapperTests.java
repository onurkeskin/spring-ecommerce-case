package com.ecommerce.shoppingservice.unit;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.UUID;

import com.ecommerce.shoppingservice.mapper.CategoryMapper;
import com.ecommerce.shoppingservice.domain.Category;
import com.ecommerce.shoppingservice.model.CategoryDto;


@ActiveProfiles("test")
public class CategoryMapperTests {
    private CategoryMapper mapper;

    @Before
    public void setUp() throws Exception {
        this.mapper = CategoryMapper.INSTANCE;
    }

    @Test
    public void DtoToEntityTest() throws Exception {
        UUID id = UUID.randomUUID();

        CategoryDto dto = new CategoryDto(id, "test");
        Category entity = mapper.CategoryDtoToCategory(dto);
        
        assertThat(entity.getId()).isEqualTo(id);
    }
    
    @Test
    public void EntityToDtoTest() throws Exception {
        UUID id = UUID.randomUUID();
        
        Category entity = new Category(id,"test", null);
        CategoryDto testCategory = mapper.CategoryToCategoryDto(entity);
        
        assertThat(testCategory.getId()).isEqualTo(id);
    }
}