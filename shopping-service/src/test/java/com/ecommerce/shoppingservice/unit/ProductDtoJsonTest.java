package com.ecommerce.shoppingservice.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.UUID;

import com.ecommerce.shoppingservice.model.ProductDto;
import com.ecommerce.shoppingservice.model.CategoryDto;

@RunWith(SpringRunner.class)
@JsonTest
@ActiveProfiles("test")
public class ProductDtoJsonTest {
    @Autowired
    private JacksonTester<ProductDto> json;

    @Test
    public void deserializeJson() throws Exception {
        UUID userUuid = UUID.randomUUID();
        UUID categoryUuid1 = UUID.randomUUID();
        UUID categoryUuid2 = UUID.randomUUID();

        ProductDto product = new ProductDto("testProduct", userUuid, 1, 10.0,
                Arrays.asList(new CategoryDto(categoryUuid1, "test"), new CategoryDto(categoryUuid2, "test2")));

        String content = "{\"name\": \"testProduct\", \"userId\": \"" + userUuid.toString() + "\", \"count\": 1, "
                + "\"price\": 10.0, \"categories\": [{\"id\":\"" + categoryUuid1.toString()
                + "\", \"name\":\"test\"},{\"id\":\"" + categoryUuid2.toString() + "\", \"name\":\"test2\"}]}";

        assertThat(this.json.parse(content)).isEqualTo(product);
        assertThat(this.json.parseObject(content).getName()).isEqualTo("testProduct");
    }
}