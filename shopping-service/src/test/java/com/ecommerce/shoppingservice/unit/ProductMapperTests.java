package com.ecommerce.shoppingservice.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import com.ecommerce.shoppingservice.mapper.ProductMapper;
import com.ecommerce.shoppingservice.domain.Product;
import com.ecommerce.shoppingservice.model.ProductDto;
import com.ecommerce.shoppingservice.domain.Category;
import com.ecommerce.shoppingservice.model.CategoryDto;

@ActiveProfiles("test")
public class ProductMapperTests {
    private ProductMapper mapper;

    @Before
    public void setUp() throws Exception {
        this.mapper = ProductMapper.INSTANCE;
    }

    @Test
    public void DtoToEntityTest() throws Exception {
        UUID userid = UUID.randomUUID();
        String productName = "test product";
        Integer count  = 5;
        Double price  = 10d;
        List<CategoryDto> categories = Arrays.asList(new CategoryDto(UUID.randomUUID(), "test"));

        ProductDto dto = new ProductDto(productName, userid, count, price, categories);
        Product entity = mapper.ProductDtoToProduct(dto);
        
        assertThat(entity.getUserId()).isEqualTo(userid);
        assertThat(entity.getName()).isEqualTo(productName);
        assertThat(entity.getCount()).isEqualTo(count);
        assertThat(entity.getPrice()).isEqualTo(price);
    }
    
    @Test
    public void EntityToDtoTest() throws Exception {
        UUID id = UUID.randomUUID();
        UUID userid = UUID.randomUUID();
        String productName = "test product";
        Integer count  = 5;
        Double price  = 10d;
        Category testCategory = new Category(UUID.randomUUID(), "test category", null);
        HashSet<Category> categories = new HashSet<>(Arrays.asList(testCategory));

        Product entity = new Product(id,userid,productName, count, price, categories);
        testCategory.setProducts(new HashSet<>(Arrays.asList(entity)));

        ProductDto dto = mapper.ProductToProductDto(entity);
        
        assertThat(dto.getUserId()).isEqualTo(userid);
        assertThat(dto.getName()).isEqualTo(productName);
        assertThat(dto.getCount()).isEqualTo(count);
        assertThat(dto.getPrice()).isEqualTo(price);
    }
}