package com.ecommerce.shoppingservice.integration;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

import com.ecommerce.shoppingservice.dao.CategoryRepository;
import com.ecommerce.shoppingservice.domain.Category;


@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles("test")
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class CategoryRepositoryTest {

    @Autowired
    private CategoryRepository CategoryRepository; 

    @Autowired
    private TestEntityManager entityManager; 

    @After
    public void clear() {
        this.entityManager.clear();
    }

    @Test
    public void findByIdShouldReturnCategory() throws Exception {
        Category cat1 = new Category(null,"electronic",null);
        Category cat2 = new Category(null,"food", null);
        Category cat3 = new Category(null,"pet", null);
        Category cat4 = new Category(null,"sport", null);

        Category toCheck = this.entityManager.persist(cat1); 
        this.entityManager.persist(cat2); 
        this.entityManager.persist(cat3); 
        this.entityManager.persist(cat4); 

        Optional<Category> against = this.CategoryRepository.findById(toCheck.getId());
        assertThat(against.isPresent()).isTrue();

        assertThat(against.get().getId()).isEqualTo(toCheck.getId());
        assertThat(against.get().getName()).isEqualTo(toCheck.getName());
    }

    @Test
    public void findAllShouldReturnAllCategories() throws Exception {
        Category cat1 = new Category(null,"electronic",null);
        Category cat2 = new Category(null,"food", null);
        Category cat3 = new Category(null,"pet", null);
        Category cat4 = new Category(null,"sport", null);

        Category toCheck1 = this.entityManager.persist(cat1); 
        Category toCheck2 = this.entityManager.persist(cat2); 
        Category toCheck3 = this.entityManager.persist(cat3); 
        Category toCheck4 = this.entityManager.persist(cat4); 

        List<Category> against = this.CategoryRepository.findAll();
        assertThat(against).hasSize(4);

        for (Category testCategory : against) {
            assertThat(testCategory.getName(), anyOf(is(toCheck1.getName()),is(toCheck2.getName()),is(toCheck3.getName()),is(toCheck4.getName())));
            assertThat(testCategory.getId(), anyOf(is(toCheck1.getId()),is(toCheck2.getId()),is(toCheck3.getId()),is(toCheck4.getId())));
        }
    }


    @Test
    public void updateCategoryShouldUpdateCategory() throws Exception {
        Category cat1 = new Category(null,"electronic",null);
        Category cat2 = new Category(null,"food", null);
        Category cat3 = new Category(null,"pet", null);
        Category cat4 = new Category(null,"sport", null);

        this.entityManager.persist(cat1); 
        this.entityManager.persist(cat2); 
        this.entityManager.persist(cat3); 
        this.entityManager.persist(cat4); 

        cat4.setName("sport2");

        Category toUpdate = this.CategoryRepository.save(cat4);

        Optional<Category> CategoryTested = this.CategoryRepository.findById(toUpdate.getId());

        assertThat(CategoryTested.isPresent()).isTrue();
        assertThat(CategoryTested.get().getId()).isEqualTo(toUpdate.getId());
        assertThat(CategoryTested.get().getName()).isEqualTo(toUpdate.getName());

        assertThat(this.CategoryRepository.findAll()).hasSize(4);
    }

    @Test
    public void deleteCategoryShouldDeleteOnlyThatCategory() throws Exception {
        Category cat1 = new Category(null,"electronic",null);
        Category cat2 = new Category(null,"food", null);
        Category cat3 = new Category(null,"pet", null);
        Category cat4 = new Category(null,"sport", null);

        this.entityManager.persist(cat1); 
        this.entityManager.persist(cat2); 
        this.entityManager.persist(cat3); 
        Category toDelete = this.entityManager.persist(cat4); 

        this.CategoryRepository.delete(cat4);

        Optional<Category> CategoryTested = this.CategoryRepository.findById(toDelete.getId());

        assertThat(CategoryTested.isPresent()).isFalse();
        
        assertThat(this.CategoryRepository.findAll()).hasSize(3);
    }
}