package com.ecommerce.shoppingservice.integration;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.hamcrest.Condition;
import org.hamcrest.collection.HasItemInArray;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;

import org.assertj.core.condition.AllOf;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

import com.ecommerce.shoppingservice.dao.CategoryRepository;
import com.ecommerce.shoppingservice.dao.ProductRepository;
import com.ecommerce.shoppingservice.domain.Category;
import com.ecommerce.shoppingservice.domain.Product;


@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles("test")
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class ProductCategoryRepositoryTest {

    @Autowired
    private CategoryRepository categoryRepository; 
    
    @Autowired
    private ProductRepository productRepository; 

    @Autowired
    private TestEntityManager entityManager; 

    @After
    public void clear() {
        this.entityManager.clear();
    }

    @Test
    public void CategoryShouldCreateProducts() throws Exception {
        UUID userId1 = UUID.randomUUID();
        UUID userId2 = UUID.randomUUID();
        
        Category cat1 = new Category(null,"electronic",null);
        Category cat2 = new Category(null,"food", null);
        Category cat3 = new Category(null,"pet", null);
        Category cat4 = new Category(null,"sport", null);

        Product product = new Product(
            null,userId1,"computer",3,10d, new HashSet<>(Arrays.asList(cat1))
        );
        Product product2 = new Product(
            null,userId1,"computer2",3,10d, new HashSet<>(Arrays.asList(cat1))
        );
        Product product3 = new Product(
            null,userId2,"noncomputer3",3,10d, new HashSet<>(Arrays.asList(cat2,cat4))
        );
        cat1.setProducts(new HashSet<>(Arrays.asList(product,product2)));
        cat2.setProducts(new HashSet<>(Arrays.asList(product3)));
        cat4.setProducts(new HashSet<>(Arrays.asList(product3)));

        cat1 = this.entityManager.persist(cat1); 
        cat2 = this.entityManager.persist(cat2); 
        cat3 = this.entityManager.persist(cat3); 
        cat4 = this.entityManager.persist(cat4);

        List<Product> toTestProduct = productRepository.findByName("noncomputer3");
        Product test = toTestProduct.iterator().next();
        assertThat(test.getCategories()).hasSize(2);
    }

}