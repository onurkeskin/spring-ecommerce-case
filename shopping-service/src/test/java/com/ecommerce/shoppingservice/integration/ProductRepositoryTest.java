package com.ecommerce.shoppingservice.integration;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.hamcrest.collection.HasItemInArray;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;

import org.assertj.core.condition.AllOf;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

import com.ecommerce.shoppingservice.dao.ProductRepository;
import com.ecommerce.shoppingservice.domain.Category;
import com.ecommerce.shoppingservice.domain.Product;


@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles("test")
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class ProductRepositoryTest {

    @Autowired
    private ProductRepository productRepository; 

    @Autowired
    private TestEntityManager entityManager; 

    @After
    public void clear() {
        this.entityManager.clear();
    }

    @Test
    public void findByNameShouldReturnProducts() throws Exception {
        UUID userId1 = UUID.randomUUID();
        UUID userId2 = UUID.randomUUID();

        Category cat1 = new Category(null,"electronic",null);
        Category cat2 = new Category(null,"food", null);
        Category cat3 = new Category(null,"pet", null);
        Category cat4 = new Category(null,"sport", null);

            Product product = new Product(
                null,userId1,"computer",3,10d, new HashSet<>(Arrays.asList(cat1))
            );
            Product product2 = new Product(
                null,userId1,"computer2",3,10d, new HashSet<>(Arrays.asList(cat1,cat3))
            );
            Product product3 = new Product(
                null,userId2,"computer3",3,10d, new HashSet<>(Arrays.asList(cat1,cat4))
            );
            
        this.entityManager.persist(product); 
        this.entityManager.persist(product2); 
        this.entityManager.persist(product3); 

        List<Product> computers = this.productRepository.findByName("computer");
        assertThat(computers).size().isEqualTo(1);
        Product computer = computers.get(0);
        assertThat(computer.getName()).isEqualTo("computer");
        assertThat(computer.getUserId()).isEqualTo(userId1);
        assertThat(computer.getCount()).isEqualTo(3);
        assertThat(computer.getPrice()).isEqualTo(10d);
        assertThat(computer.getCategories().iterator().next()).isEqualTo(cat1);
    }

    @Test
    public void findByUserIdShouldReturnProducts() throws Exception {
        UUID userId1 = UUID.randomUUID();
        UUID userId2 = UUID.randomUUID();

        Category cat1 = new Category(null,"electronic",null);
        Category cat2 = new Category(null,"food", null);
        Category cat3 = new Category(null,"pet", null);
        Category cat4 = new Category(null,"sport", null);

        Product product = new Product(
            null,userId1,"computer",3,10d, new HashSet<>(Arrays.asList(cat1))
        );
        Product product2 = new Product(
            null,userId1,"computer2",3,10d, new HashSet<>(Arrays.asList(cat3))
        );
        Product product3 = new Product(
            null,userId2,"computer3",3,10d, new HashSet<>(Arrays.asList(cat1,cat4))
        );

        this.entityManager.persist(product); 
        this.entityManager.persist(product2); 
        this.entityManager.persist(product3); 

        List<Product> userProducts = this.productRepository.findByUserId(userId1);
        assertThat(userProducts).size().isEqualTo(2);
        for (Product testProduct : userProducts) {
            assertThat(testProduct.getName(), anyOf(is("computer"),is("computer2")));
            assertThat(testProduct.getUserId()).isEqualTo(userId1);
            assertThat(testProduct.getCount()).isEqualTo(3);
            assertThat(testProduct.getPrice()).isEqualTo(10d);
            for (Category category : testProduct.getCategories()) {
                assertThat(category.getName(), anyOf(is("electronic"),is("pet")));
            }
        }
    }

    @Test
    public void findByCategoryIdReturnProductsWithCategory() throws Exception {
        UUID userId1 = UUID.randomUUID();
        UUID userId2 = UUID.randomUUID();

        Category cat1 = new Category(null,"electronic",null);
        Category cat2 = new Category(null,"food", null);
        Category cat3 = new Category(null,"pet", null);
        Category cat4 = new Category(null,"sport", null);

        Product product = new Product(
            null,userId1,"computer",3,10d, new HashSet<>(Arrays.asList(cat1))
        );
        Product product2 = new Product(
            null,userId1,"computer2",3,10d, new HashSet<>(Arrays.asList(cat1))
        );
        Product product3 = new Product(
            null,userId2,"noncomputer3",3,10d, new HashSet<>(Arrays.asList(cat2,cat4))
        );
        cat1.setProducts(new HashSet<>(Arrays.asList(product,product2)));
        cat2.setProducts(new HashSet<>(Arrays.asList(product3)));
        cat4.setProducts(new HashSet<>(Arrays.asList(product3)));

        cat1 = this.entityManager.persist(cat1); 
        cat2 = this.entityManager.persist(cat2); 
        cat3 = this.entityManager.persist(cat3); 
        cat4 = this.entityManager.persist(cat4);

        List<Product> userProducts = this.productRepository.findByCategories_Id(cat1.getId());
        assertThat(userProducts).size().isEqualTo(2);
        for (Product testProduct : userProducts) {
            assertThat(testProduct.getName(), anyOf(is("computer"),is("computer2")));
            assertThat(testProduct.getUserId()).isEqualTo(userId1);
            assertThat(testProduct.getCount()).isEqualTo(3);
            assertThat(testProduct.getPrice()).isEqualTo(10d);
            for (Category category : testProduct.getCategories()) {
                assertThat(category.getName()).isEqualTo("electronic");
            }
        }
    }

    @Test
    public void updateProductShouldUpdateProduct() throws Exception {
        UUID userId1 = UUID.randomUUID();
        UUID userId2 = UUID.randomUUID();
        UUID userId3 = UUID.randomUUID();

        Category cat1 = new Category(null,"electronic",null);
        Category cat2 = new Category(null,"food", null);
        Category cat3 = new Category(null,"pet", null);
        Category cat4 = new Category(null,"sport", null);

        this.entityManager.persist(cat1); 
        this.entityManager.persist(cat2); 
        this.entityManager.persist(cat3); 
        this.entityManager.persist(cat4); 

        Product product = new Product(
            null,userId1,"computer",3,10d, new HashSet<>(Arrays.asList(cat1))
        );
        Product product2 = new Product(
            null,userId2,"computer2",3,10d, new HashSet<>(Arrays.asList(cat1))
        );
        Product product3 = new Product(
            null,userId3,"noncomputer3",3,10d, new HashSet<>(Arrays.asList(cat2,cat4))
        );

        Product toUpdate = this.entityManager.persist(product); 
        this.entityManager.persist(product2); 
        this.entityManager.persist(product3); 

        toUpdate.setCount(5);

        this.productRepository.save(toUpdate);

        Optional<Product> productTested = this.productRepository.findById(toUpdate.getId());

        assertThat(productTested.isPresent()).isTrue();
        assertThat(productTested.get().getUserId()).isEqualTo(toUpdate.getUserId());
        assertThat(productTested.get().getCount()).isEqualTo(toUpdate.getCount());
        assertThat(productTested.get().getPrice()).isEqualTo(toUpdate.getPrice());

        assertThat(this.productRepository.findAll()).hasSize(3);
    }

    @Test
    public void deleteProductShouldDeleteOnlyThatProduct() throws Exception {
        UUID userId1 = UUID.randomUUID();
        UUID userId2 = UUID.randomUUID();

        Category cat1 = new Category(null,"electronic",null);
        Category cat2 = new Category(null,"food", null);
        Category cat3 = new Category(null,"pet", null);
        Category cat4 = new Category(null,"sport", null);

        this.entityManager.persist(cat1); 
        this.entityManager.persist(cat2); 
        this.entityManager.persist(cat3); 
        this.entityManager.persist(cat4); 

        Product product = new Product(
            null,userId1,"computer",3,10d, new HashSet<>(Arrays.asList(cat1))
        );
        Product product2 = new Product(
            null,userId1,"computer2",3,10d, new HashSet<>(Arrays.asList(cat1))
        );
        Product product3 = new Product(
            null,userId2,"noncomputer3",3,10d, new HashSet<>(Arrays.asList(cat2,cat4))
        );

        Product toDelete = this.entityManager.persist(product); 
        this.entityManager.persist(product2); 
        this.entityManager.persist(product3); 

        this.productRepository.delete(product);
        Optional<Product> productTested = this.productRepository.findById(toDelete.getId());

        assertThat(productTested.isPresent()).isFalse();
        
        assertThat(this.productRepository.findAll()).hasSize(2);
    }
}