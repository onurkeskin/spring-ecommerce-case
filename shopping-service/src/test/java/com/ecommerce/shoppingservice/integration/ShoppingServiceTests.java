package com.ecommerce.shoppingservice.integration;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import org.springframework.boot.test.mock.mockito.MockBean;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.when;
import static org.mockito.BDDMockito.any;


import com.ecommerce.shoppingservice.dao.CategoryRepository;
import com.ecommerce.shoppingservice.dao.ProductRepository;
import com.ecommerce.shoppingservice.domain.Category;
import com.ecommerce.shoppingservice.domain.Product;
import com.ecommerce.shoppingservice.model.exceptions.ProductNotFoundException;
import com.ecommerce.shoppingservice.service.impl.ShoppingServiceImpl;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class ShoppingServiceTests {

    @MockBean
    private ProductRepository productRepository;

    @MockBean
    private CategoryRepository categoryRepository;

    private ShoppingServiceImpl shoppingServiceImpl;

    @Before
    public void before() {
        shoppingServiceImpl = new ShoppingServiceImpl(productRepository, categoryRepository);
    }

    @Test
    public void getAllCategoriesShouldReturnCategories() throws Exception {
        Category cat1 = new Category(null, "electronic", null);
        Category cat2 = new Category(null, "food", null);
        Category cat3 = new Category(null, "pet", null);
        Category cat4 = new Category(null, "sport", null);

        given(this.categoryRepository.findAll()).willReturn(Arrays.asList(cat1, cat2, cat3, cat4));

        List<Category> catogories = this.shoppingServiceImpl.getAllCategories();
        assertThat(catogories).size().isEqualTo(4);

        for (Category category : catogories) {
            assertThat(category.getName(),
                    anyOf(is(cat1.getName()), is(cat2.getName()), is(cat3.getName()), is(cat4.getName())));
        }
    }

    @Test
    public void addCategoryDuplicateWillNull() throws Exception {
        Category cat1 = new Category(null, "electronic", null);

        when(this.categoryRepository.save(cat1)).thenReturn(cat1).thenThrow(new IllegalArgumentException("test"));

        Category category = this.shoppingServiceImpl.createCategory(cat1);
        assertThat(category).isEqualTo(cat1);

        Category category2 = this.shoppingServiceImpl.createCategory(cat1);
        assertThat(category2).isEqualTo(null);
    }

    @Test
    public void getAllProductsShouldReturnProducts() throws Exception {
        Category cat1 = new Category(null, "electronic", null);
        Category cat2 = new Category(null, "food", null);
        Category cat3 = new Category(null, "pet", null);
        Category cat4 = new Category(null, "sport", null);

        Product product = new Product(null, UUID.randomUUID(), "computer", 3, 10d, new HashSet<>(Arrays.asList(cat1)));
        Product product2 = new Product(null, UUID.randomUUID(), "computer2", 3, 10d,
                new HashSet<>(Arrays.asList(cat1, cat3)));
        Product product3 = new Product(null, UUID.randomUUID(), "computer3", 3, 10d,
                new HashSet<>(Arrays.asList(cat1, cat4)));

        given(this.productRepository.findAll()).willReturn(Arrays.asList(product, product2, product3));

        Iterable<Product> testProducts = this.shoppingServiceImpl.getAllProducts();
        assertThat(testProducts).size().isEqualTo(3);

        for (Product testProduct : testProducts) {
            assertThat(testProduct.getName(),
                    anyOf(is(product.getName()), is(product2.getName()), is(product3.getName())));
            assertThat(testProduct.getUserId(),
                    anyOf(is(product.getUserId()), is(product2.getUserId()), is(product3.getUserId())));

            for (Category category : testProduct.getCategories()) {
                assertThat(category, anyOf(is(cat1), is(cat2), is(cat3), is(cat4)));
            }
        }
    }

    @Test
    public void getProductsByCategoryPaginatedShouldReturnProductsByCategory() throws Exception {
        UUID searchID = UUID.randomUUID();

        Category cat1 = new Category(searchID, "electronic", null);
        Category cat3 = new Category(UUID.randomUUID(), "pet", null);

        Product product = new Product(null, UUID.randomUUID(), "computer", 3, 10d, new HashSet<>(Arrays.asList(cat1)));
        Product product2 = new Product(null, UUID.randomUUID(), "computer2", 3, 10d,
                new HashSet<>(Arrays.asList(cat1, cat3)));

        given(this.productRepository.findByCategories_Id(searchID)).willReturn(Arrays.asList(product, product2));

        Iterable<Product> testProducts = this.shoppingServiceImpl.getProductsByCategoryPaginated(searchID, null, 0, 0);
        assertThat(testProducts).size().isEqualTo(2);

        for (Product testProduct : testProducts) {
            assertThat(testProduct.getName(), anyOf(is(product.getName()), is(product2.getName())));
            assertThat(testProduct.getUserId(), anyOf(is(product.getUserId()), is(product2.getUserId())));

            assertThat(testProduct.getCategories(), hasItem(cat1));
        }
    }

    @Test
    public void getProductByIdShouldReturnProduct() throws Exception {
        Category cat1 = new Category(UUID.randomUUID(), "electronic", null);
        Category cat3 = new Category(UUID.randomUUID(), "pet", null);

        UUID searchID = UUID.randomUUID();

        Product product = new Product(searchID, UUID.randomUUID(), "computer", 3, 10d,
                new HashSet<>(Arrays.asList(cat1)));
        Product product2 = new Product(UUID.randomUUID(), UUID.randomUUID(), "computer2", 3, 10d,
                new HashSet<>(Arrays.asList(cat1, cat3)));

        given(this.productRepository.findById(searchID)).willReturn(Optional.of(product));

        Optional<Product> testProductO = this.shoppingServiceImpl.getProduct(searchID);

        assertThat(testProductO.isPresent()).isTrue();
        Product testProduct = testProductO.get();

        assertThat(testProduct.getName(), anyOf(is(product.getName()), is(product2.getName())));
        assertThat(testProduct.getUserId(), anyOf(is(product.getUserId()), is(product2.getUserId())));
        assertThat(testProduct.getCategories(), hasItem(cat1));
    }

    @Test
    public void createProductShouldCreate() throws Exception {
        Category cat1 = new Category(UUID.randomUUID(), "electronic", null);
        Category cat2 = new Category(UUID.randomUUID(), "food", null);

        UUID searchID = UUID.randomUUID();

        Product product = new Product(searchID, UUID.randomUUID(), "computer", 1, 10d,
                new HashSet<>(Arrays.asList(cat1)));
        Product product3 = new Product(UUID.randomUUID(), UUID.randomUUID(), "computer", 3, 10d,
                new HashSet<>(Arrays.asList(cat2)));

        when(this.productRepository.save(any())).thenReturn(product).thenReturn(product3);
        when(this.productRepository.findById(searchID)).thenReturn(Optional.empty()).thenReturn(Optional.of(product));

        Product tocreateProduct1 = this.shoppingServiceImpl.createProduct(product);
        assertThat(tocreateProduct1).isEqualTo(product);
    }

    @Test
    public void updateProductShouldReturnUpdatedProduct() throws Exception {
        Category cat1 = new Category(UUID.randomUUID(), "electronic", null);
        Category cat2 = new Category(UUID.randomUUID(), "food", null);

        UUID searchID = UUID.randomUUID();

        Product product = new Product(searchID, UUID.randomUUID(), "computer", 1, 10d,
                new HashSet<>(Arrays.asList(cat1)));
        Product productUpdated = new Product(searchID, UUID.randomUUID(), "computer2", 2, 11d,
                new HashSet<>(Arrays.asList(cat2)));

        when(this.productRepository.save(any())).thenReturn(productUpdated);
        when(this.productRepository.findById(searchID)).thenReturn(Optional.of(product));

        Product updatedProduct = this.shoppingServiceImpl.updateProduct(productUpdated);
        assertThat(updatedProduct.getId()).isEqualTo(productUpdated.getId());
        assertThat(updatedProduct.getName()).isEqualTo(productUpdated.getName());
        assertThat(updatedProduct.getPrice()).isEqualTo(productUpdated.getPrice());
        assertThat(updatedProduct.getUserId()).isEqualTo(productUpdated.getUserId());
        assertThat(updatedProduct.getCount()).isEqualTo(productUpdated.getCount());
    }

    @Test
    public void updateProductShouldThrowWhenUpdatingNonExistentProduct() throws Exception {
        Category cat2 = new Category(UUID.randomUUID(), "food", null);

        UUID searchID = UUID.randomUUID();

        Product productUpdated = new Product(searchID, UUID.randomUUID(), "computer2", 2, 11d,
                new HashSet<>(Arrays.asList(cat2)));

        when(this.productRepository.findById(searchID)).thenReturn(Optional.empty());

        assertThrows(ProductNotFoundException.class, () -> {
            this.shoppingServiceImpl.updateProduct(productUpdated);
        });
    }

    @Test
    public void deleteProductShouldReturDeletedProduct() throws Exception {
        Category cat1 = new Category(UUID.randomUUID(), "electronic", null);

        UUID searchID = UUID.randomUUID();

        Product product = new Product(searchID, UUID.randomUUID(), "computer", 1, 10d,
                new HashSet<>(Arrays.asList(cat1)));

        when(this.productRepository.findById(searchID)).thenReturn(Optional.of(product));

        Product deletedProduct = this.shoppingServiceImpl.deleteProduct(product.getId());
        assertThat(deletedProduct.getId()).isEqualTo(product.getId());
        assertThat(deletedProduct.getName()).isEqualTo(product.getName());
        assertThat(deletedProduct.getPrice()).isEqualTo(product.getPrice());
        assertThat(deletedProduct.getUserId()).isEqualTo(product.getUserId());
        assertThat(deletedProduct.getCount()).isEqualTo(product.getCount());
    }

    @Test
    public void deleteProductShouldThrowWhenDeletingNonExistentProduct() throws Exception {
        Category cat1 = new Category(UUID.randomUUID(), "electronic", null);

        UUID searchID = UUID.randomUUID();

        Product product = new Product(searchID, UUID.randomUUID(), "computer", 1, 10d,
                new HashSet<>(Arrays.asList(cat1)));

        when(this.productRepository.findById(searchID)).thenReturn(Optional.empty());

        assertThrows(ProductNotFoundException.class, () -> {
            this.shoppingServiceImpl.deleteProduct(product.getId());
        });
    }
}