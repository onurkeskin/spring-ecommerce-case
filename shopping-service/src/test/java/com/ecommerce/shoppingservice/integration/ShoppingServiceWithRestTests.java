package com.ecommerce.shoppingservice.integration;

import org.hamcrest.collection.HasItemInArray;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.util.ArrayList;
import java.util.List;

import com.ecommerce.shoppingservice.service.*;
import com.ecommerce.shoppingservice.dao.*;
import com.ecommerce.shoppingservice.domain.Category;

@RunWith(SpringRunner.class)
@SpringBootTest
// @RestClientTest({ shoppingService.class })
@AutoConfigureWebClient(registerRestTemplate = true)
@ActiveProfiles("test")
public class ShoppingServiceWithRestTests {

        @Autowired
        private ShoppingService shoppingService;

        private MockRestServiceServer server;

        @Autowired
        RestTemplate restTemplate;

        @Before
        public void setUp() {
                server = MockRestServiceServer.createServer(restTemplate);
        }
        
        @Test
        public void PostWithAuthShouldPostNewItemOnlyAuthed() {
                // this.server.expect(requestTo("http://user-service/rest/auth/v1/"))

                // Iterable<Category> cat = shoppingService.getAllCategories();
        }
}
