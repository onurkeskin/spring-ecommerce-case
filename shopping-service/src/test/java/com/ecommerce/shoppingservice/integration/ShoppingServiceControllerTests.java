package com.ecommerce.shoppingservice.integration;

import java.util.HashSet;
import java.util.Optional;
import java.util.UUID;
import java.security.Principal;
import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.ecommerce.shoppingservice.controller.ShoppingServiceController;
import com.ecommerce.shoppingservice.service.ShoppingService;
import com.ecommerce.shoppingservice.model.exceptions.ProductNotFoundException;
import com.ecommerce.shoppingservice.domain.*;

@RunWith(SpringRunner.class)
@WebMvcTest(ShoppingServiceController.class)
@AutoConfigureMockMvc(addFilters = false)
@ActiveProfiles("test")
public class ShoppingServiceControllerTests {

        @Autowired
        private MockMvc mvc;

        @MockBean
        private ShoppingService ShoppingService;

        @Test
        public void getCategoriesShouldReturnCategoriesJson() throws Exception {

                UUID first = UUID.randomUUID();
                UUID second = UUID.randomUUID();
                UUID third = UUID.randomUUID();
                UUID fourth = UUID.randomUUID();

                String asJson = "[{\"id\":\"" + first + "\",\"name\":\"electronic\"},{\"id\":\"" + second
                                + "\",\"name\":\"pet\"},{\"id\":\"" + third + "\",\"name\":\"food\"},{\"id\":\""
                                + fourth + "\",\"name\":\"sports\"}]";

                given(this.ShoppingService.getAllCategories()).willReturn(Arrays.asList(
                                new Category(first, "electronic", null), new Category(second, "pet", null),
                                new Category(third, "food", null), new Category(fourth, "sports", null)));

                this.mvc.perform(get("/rest/shopping/v1/category").accept(MediaType.APPLICATION_JSON))
                                .andExpect(status().isOk()).andExpect(content().json(asJson));
        }

        @Test
        public void addCategoryShouldReturnAddedCategoryJson() throws Exception {
                UUID first = UUID.randomUUID();

                String requestBody = "{\"name\": \"electronic\"}";
                String asJson = "{\"id\":\"" + first + "\",\"name\":\"electronic\"}";
                Category toAdd = new Category(first, "electronic", null);

                given(this.ShoppingService.createCategory(any())).willReturn(toAdd);

                this.mvc.perform(post("/rest/shopping/v1/category").contentType(MediaType.APPLICATION_JSON)
                                .content(requestBody).accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                                .andExpect(content().json(asJson));
        }

        @Test
        public void getProductsShouldReturnProductsJson() throws Exception {
                UUID firstProductId = UUID.randomUUID();
                UUID secondProductId = UUID.randomUUID();
                UUID thirdProductId = UUID.randomUUID();

                UUID firstUserId = UUID.randomUUID();
                UUID secondUserId = firstUserId;
                UUID thirdUserId = UUID.randomUUID();

                UUID firstCategoryId = UUID.randomUUID();
                UUID secondCategoryId = UUID.randomUUID();
                UUID thirdCategoryId = UUID.randomUUID();
                UUID fourthCategoryId = UUID.randomUUID();

                String asJson = "[{\"id\":\"" + firstProductId + "\",\"userId\":\"" + firstUserId
                                + "\",\"name\":\"computer\",\"count\":3,\"price\":10.0,\"categories\":[{\"id\":\""
                                + firstCategoryId + "\",\"name\":\"electronic\"}]},{\"id\":\"" + secondProductId
                                + "\",\"userId\":\"" + secondUserId
                                + "\",\"name\":\"computer2\",\"count\":3,\"price\":10.0,\"categories\":[{\"id\":\""
                                + thirdCategoryId + "\",\"name\":\"pet\"},{\"id\":\"" + firstCategoryId
                                + "\",\"name\":\"electronic\"}]},{\"id\":\"" + thirdProductId + "\",\"userId\":\""
                                + thirdUserId
                                + "\",\"name\":\"computer3\",\"count\":3,\"price\":10.0,\"categories\":[{\"id\":\""
                                + fourthCategoryId + "\",\"name\":\"sport\"},{\"id\":\"" + firstCategoryId
                                + "\",\"name\":\"electronic\"}]}]";

                Category cat1 = new Category(firstCategoryId, "electronic", null);
                Category cat2 = new Category(secondCategoryId, "food", null);
                Category cat3 = new Category(thirdCategoryId, "pet", null);
                Category cat4 = new Category(fourthCategoryId, "sport", null);

                Product product = new Product(firstProductId, firstUserId, "computer", 3, 10d,
                                new HashSet<>(Arrays.asList(cat1)));
                Product product2 = new Product(secondProductId, secondUserId, "computer2", 3, 10d,
                                new HashSet<>(Arrays.asList(cat1, cat3)));
                Product product3 = new Product(thirdProductId, thirdUserId, "computer3", 3, 10d,
                                new HashSet<>(Arrays.asList(cat1, cat4)));

                given(this.ShoppingService.getAllProducts()).willReturn(Arrays.asList(product, product2, product3));

                this.mvc.perform(get("/rest/shopping/v1/products").contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                                .andExpect(content().json(asJson));
        }

        @Test
        public void searchProductsShouldReturnSearchedCategoryProductsJson() throws Exception {
                UUID firstProductId = UUID.randomUUID();
                UUID secondProductId = UUID.randomUUID();
                UUID thirdProductId = UUID.randomUUID();

                UUID firstUserId = UUID.randomUUID();
                UUID secondUserId = firstUserId;
                UUID thirdUserId = UUID.randomUUID();

                UUID firstCategoryId = UUID.randomUUID();
                UUID secondCategoryId = UUID.randomUUID();
                UUID thirdCategoryId = UUID.randomUUID();
                UUID fourthCategoryId = UUID.randomUUID();

                String asJson = "[{\"id\":\"" + firstProductId + "\",\"userId\":\"" + firstUserId
                                + "\",\"name\":\"computer\",\"count\":3,\"price\":10.0,\"categories\":[{\"id\":\""
                                + firstCategoryId + "\",\"name\":\"electronic\"}]},{\"id\":\"" + secondProductId
                                + "\",\"userId\":\"" + secondUserId
                                + "\",\"name\":\"computer2\",\"count\":3,\"price\":10.0,\"categories\":[{\"id\":\""
                                + thirdCategoryId + "\",\"name\":\"pet\"},{\"id\":\"" + firstCategoryId
                                + "\",\"name\":\"electronic\"}]},{\"id\":\"" + thirdProductId + "\",\"userId\":\""
                                + thirdUserId
                                + "\",\"name\":\"computer3\",\"count\":3,\"price\":10.0,\"categories\":[{\"id\":\""
                                + fourthCategoryId + "\",\"name\":\"sport\"},{\"id\":\"" + firstCategoryId
                                + "\",\"name\":\"electronic\"}]}]";

                Category cat1 = new Category(firstCategoryId, "electronic", null);
                Category cat2 = new Category(secondCategoryId, "food", null);
                Category cat3 = new Category(thirdCategoryId, "pet", null);
                Category cat4 = new Category(fourthCategoryId, "sport", null);

                Product product = new Product(firstProductId, firstUserId, "computer", 3, 10d,
                                new HashSet<>(Arrays.asList(cat1)));
                Product product2 = new Product(secondProductId, secondUserId, "computer2", 3, 10d,
                                new HashSet<>(Arrays.asList(cat1, cat3)));
                Product product3 = new Product(thirdProductId, thirdUserId, "computer3", 3, 10d,
                                new HashSet<>(Arrays.asList(cat1, cat4)));

                given(this.ShoppingService.getProductsByCategoryPaginated(any(), anyString(), anyInt(), anyInt())).willReturn(Arrays.asList(product, product2, product3));
                
                MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
                queryParams.put("category", Arrays.asList(firstCategoryId.toString()));
                this.mvc.perform(get("/rest/shopping/v1/products/search").queryParams(queryParams).contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                                .andExpect(content().json(asJson));
        }

        @Test
        public void getProductsWithCateShouldReturnProductsCategory() throws Exception {
                UUID firstProductId = UUID.randomUUID();

                UUID firstUserId = UUID.randomUUID();

                UUID firstCategoryId = UUID.randomUUID();

                Principal mockPrincipal = Mockito.mock(Principal.class);
                Mockito.when(mockPrincipal.getName()).thenReturn(firstUserId.toString());

                String asJson = "{\"id\":\"" + firstProductId + "\",\"userId\":\"" + firstUserId
                                + "\",\"name\":\"computer\",\"count\":3,\"price\":10.0,\"categories\":[{\"id\":\""
                                + firstCategoryId + "\",\"name\":\"electronic\"}]}";

                String requestBody = "{\"name\":\"computer\",\"count\":3,\"price\":10.0,\"categories\":[{\"id\":\""
                                + firstCategoryId + "\"}]}";

                Category cat1 = new Category(firstCategoryId, "electronic", null);

                Product product = new Product(firstProductId, firstUserId, "computer", 3, 10d,
                                new HashSet<>(Arrays.asList(cat1)));

                given(this.ShoppingService.createProduct(any())).willReturn(product);

                this.mvc.perform(post("/rest/shopping/v1/products").characterEncoding("utf-8").principal(mockPrincipal)
                                .contentType(MediaType.APPLICATION_JSON).content(requestBody)
                                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                                .andExpect(content().json(asJson));
        }

        @Test
        public void getProductByIdShouldReturnProductIfFound() throws Exception {
                UUID firstProductId = UUID.randomUUID();

                UUID firstUserId = UUID.randomUUID();

                UUID firstCategoryId = UUID.randomUUID();

                Principal mockPrincipal = Mockito.mock(Principal.class);
                Mockito.when(mockPrincipal.getName()).thenReturn(firstUserId.toString());

                String asJson = "{\"id\":\"" + firstProductId + "\",\"userId\":\"" + firstUserId
                                + "\",\"name\":\"computer\",\"count\":3,\"price\":10.0,\"categories\":[{\"id\":\""
                                + firstCategoryId + "\",\"name\":\"electronic\"}]}";

                Category cat1 = new Category(firstCategoryId, "electronic", null);

                Product product = new Product(firstProductId, firstUserId, "computer", 3, 10d,
                                new HashSet<>(Arrays.asList(cat1)));

                given(this.ShoppingService.getProduct(any())).willReturn(Optional.of(product));

                this.mvc.perform(get("/rest/shopping/v1/products/"+firstProductId.toString()).characterEncoding("utf-8").principal(mockPrincipal)
                                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                                .andExpect(content().json(asJson));
        }

        @Test
        public void getProductByIdShouldReturnNotFoundIfNonExistent() throws Exception {
                UUID firstProductId = UUID.randomUUID();

                UUID firstUserId = UUID.randomUUID();

                UUID firstCategoryId = UUID.randomUUID();

                Principal mockPrincipal = Mockito.mock(Principal.class);
                Mockito.when(mockPrincipal.getName()).thenReturn(firstUserId.toString());


                given(this.ShoppingService.getProduct(any())).willReturn(Optional.empty());

                this.mvc.perform(get("/rest/shopping/v1/products/"+firstProductId.toString()).characterEncoding("utf-8").principal(mockPrincipal)
                                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
        }

        @Test
        public void updateProductShouldReturnUpdatedProduct() throws Exception {
                UUID firstProductId = UUID.randomUUID();

                UUID firstUserId = UUID.randomUUID();

                UUID firstCategoryId = UUID.randomUUID();

                Principal mockPrincipal = Mockito.mock(Principal.class);
                Mockito.when(mockPrincipal.getName()).thenReturn(firstUserId.toString());

                String asJson = "{\"id\":\"" + firstProductId + "\",\"userId\":\"" + firstUserId
                                + "\",\"name\":\"computer\",\"count\":3,\"price\":10.0,\"categories\":[{\"id\":\""
                                + firstCategoryId + "\",\"name\":\"electronic\"}]}";

                String requestBody = "{\"name\":\"computer\",\"count\":3,\"price\":10.0,\"categories\":[{\"id\":\""
                                + firstCategoryId + "\"}]}";

                Category cat1 = new Category(firstCategoryId, "electronic", null);

                Product product = new Product(firstProductId, firstUserId, "computer", 3, 10d,
                                new HashSet<>(Arrays.asList(cat1)));

                given(this.ShoppingService.updateProduct(any())).willReturn(product);

                this.mvc.perform(put("/rest/shopping/v1/products/" + firstProductId.toString())
                                .characterEncoding("utf-8").principal(mockPrincipal)
                                .contentType(MediaType.APPLICATION_JSON).content(requestBody)
                                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                                .andExpect(content().json(asJson));
        }

        @Test
        public void updateProductReturnShouldNotFoundOnNonExistentJson() throws Exception {
                UUID firstProductId = UUID.randomUUID();

                UUID firstUserId = UUID.randomUUID();

                UUID firstCategoryId = UUID.randomUUID();

                String requestBody = "{\"name\":\"computer\",\"count\":3,\"price\":10.0,\"categories\":[{\"id\":\""
                                + firstCategoryId + "\"}]}";

                given(this.ShoppingService.updateProduct(any())).willThrow(ProductNotFoundException.class);

                this.mvc.perform(put("/rest/shopping/v1/products/" + firstProductId.toString())
                                .characterEncoding("utf-8").contentType(MediaType.APPLICATION_JSON).content(requestBody)
                                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
        }

        @Test
        public void deleteProductShouldReturnDeletedProduct() throws Exception {
                UUID firstProductId = UUID.randomUUID();

                UUID firstUserId = UUID.randomUUID();

                UUID firstCategoryId = UUID.randomUUID();

                String asJson = "{\"id\":\"" + firstProductId + "\",\"userId\":\"" + firstUserId
                                + "\",\"name\":\"computer\",\"count\":3,\"price\":10.0,\"categories\":[{\"id\":\""
                                + firstCategoryId + "\",\"name\":\"electronic\"}]}";

                Category cat1 = new Category(firstCategoryId, "electronic", null);

                Product product = new Product(firstProductId, firstUserId, "computer", 3, 10d,
                                new HashSet<>(Arrays.asList(cat1)));

                given(this.ShoppingService.deleteProduct(firstProductId)).willReturn(product);

                this.mvc.perform(delete("/rest/shopping/v1/products/" + firstProductId.toString())
                                .characterEncoding("utf-8").accept(MediaType.APPLICATION_JSON))
                                .andExpect(status().isOk()).andExpect(content().json(asJson));
        }

        @Test
        public void deleteProductReturnShouldNotFoundOnNonExistentJson() throws Exception {
                UUID firstProductId = UUID.randomUUID();

                given(this.ShoppingService.deleteProduct(any())).willThrow(ProductNotFoundException.class);

                this.mvc.perform(delete("/rest/shopping/v1/products/" + firstProductId.toString())
                                .characterEncoding("utf-8").accept(MediaType.APPLICATION_JSON))
                                .andExpect(status().isNotFound());
        }
}