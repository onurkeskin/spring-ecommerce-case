package com.ecommerce.authorizationservice.unit;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.containsString;

import com.ecommerce.authorizationservice.model.LoginCredentialsDto;

import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
public class LoginCredentialsDtoTest {

    @Test
    public void LoginCredentialsDtoNoArgConsturctibleToNullFieldsTest() throws Exception {
        assertDoesNotThrow(() -> {
            LoginCredentialsDto dto = new LoginCredentialsDto();
            assertThat(dto.getUsername()).isNull();
            assertThat(dto.getPassword()).isNull();
        });
    }

    @Test
    public void LoginCredentialsDtoAllArgsConsturctibleTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testUsername = "testUser";
            String testPassword = "testPassword";

            LoginCredentialsDto dto = new LoginCredentialsDto(testUsername, testPassword);
            assertThat(dto.getUsername()).isEqualTo(testUsername);
            assertThat(dto.getPassword()).isEqualTo(testPassword);
        });
    }

    @Test
    public void GetUsernameTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testUsername = "testUsername";

            LoginCredentialsDto dto = new LoginCredentialsDto(testUsername, "testPassword");
            assertThat(dto.getUsername()).isEqualTo(testUsername);
        });
    }

    @Test
    public void GetPasswordTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testPassword = "testPassword";

            LoginCredentialsDto dto = new LoginCredentialsDto("testUsername", testPassword);
            assertThat(dto.getPassword()).isEqualTo(testPassword);
        });
    }

    @Test
    public void SetUsernameUpdatesUsernameTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testUsername = "testUsername";
            String testUsername2 = "testUsername2";

            LoginCredentialsDto dto = new LoginCredentialsDto(testUsername, "testPassword");
            assertThat(dto.getUsername()).isEqualTo(testUsername);
            dto.setUsername(testUsername2);
            assertThat(dto.getUsername()).isEqualTo(testUsername2);
        });
    }

    @Test
    public void SetPasswordUpdatesPasswordTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testPassword = "testPassword";
            String testPassword2 = "testPassword2";

            LoginCredentialsDto dto = new LoginCredentialsDto("testUsername", testPassword);
            assertThat(dto.getPassword()).isEqualTo(testPassword);
            dto.setPassword(testPassword2);
            assertThat(dto.getPassword()).isEqualTo(testPassword2);
        });
    }

    @Test
    public void AccountReferenceEqualityTest() throws Exception {
        assertDoesNotThrow(() -> {
            LoginCredentialsDto dto = new LoginCredentialsDto("testUsername", "testPassword");
            assertThat(dto.equals(dto)).isTrue();
        });
    }

    @Test
    public void AccountDeepEqualityTest() throws Exception {
        assertDoesNotThrow(() -> {
            LoginCredentialsDto dto = new LoginCredentialsDto("testUsername", "testPassword");
            LoginCredentialsDto dto2 = new LoginCredentialsDto("testUsername", "testPassword");
            LoginCredentialsDto dto3 = new LoginCredentialsDto("testUsername2", "testPassword");
            assertThat(dto.equals(dto2)).isTrue();
            assertThat(dto.equals(dto3)).isFalse();
        });
    }

    @Test
    public void AccountHashCodeSameRefMustSameResultTest() throws Exception {
        assertDoesNotThrow(() -> {
            LoginCredentialsDto dto = new LoginCredentialsDto("testUsername", "testPassword");
            assertThat(dto.hashCode()).isEqualTo(dto.hashCode());
        });
    }

    @Test
    public void AccountHashCodeDifferentRefMustSameFieldsMustBeSameTest() throws Exception {
        assertDoesNotThrow(() -> {
            LoginCredentialsDto dto = new LoginCredentialsDto("testUsername", "testPassword");
            LoginCredentialsDto dto2 = new LoginCredentialsDto("testUsername", "testPassword");
            assertThat(dto.hashCode()).isEqualTo(dto2.hashCode());
        });
    }

    @Test
    public void ToStringTest() throws Exception {
        assertDoesNotThrow(() -> {
            LoginCredentialsDto dto = new LoginCredentialsDto("testUsername", "testPassword");
            assertThat(dto.toString(),
                    CoreMatchers.allOf(containsString("testUsername"), containsString("testPassword")));
        });
    }
}
