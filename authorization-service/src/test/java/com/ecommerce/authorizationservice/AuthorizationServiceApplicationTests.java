package com.ecommerce.authorizationservice;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@SpringBootTest
class AuthorizationServiceApplicationTests {

	@Test
	void contextLoads() {
	}

}
