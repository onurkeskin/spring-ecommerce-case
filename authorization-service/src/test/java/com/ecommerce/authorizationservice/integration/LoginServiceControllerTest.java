package com.ecommerce.authorizationservice.integration;

import com.ecommerce.authorizationservice.controller.LoginController;
import com.ecommerce.authorizationservice.service.LoginService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@WebMvcTest(LoginController.class)
@AutoConfigureMockMvc(addFilters = false)
@ActiveProfiles("test")
public class LoginServiceControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LoginService loginService;

    @Test
    public void LoginOnValidCredientialsShouldReturnToken() throws Exception {
        String testUsername = "testUsername";
        String testPassword = "testPassword";
        final String testToken = "{\"token\": \"someToken\"}}";

        String requestBody = "{\"username\":\"" + testUsername + "\",\"password\":\""
                + testPassword + "\"}";

        given(this.loginService.login(anyString(), anyString()))
                .willReturn(ResponseEntity.ok().body(testToken));

        this.mvc.perform(post("/rest/auth/v1/login").characterEncoding("utf-8")
                .contentType(MediaType.APPLICATION_JSON).content(requestBody).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().string(testToken));
    }

    @Test
    public void LoginOnInvalidCredientialsShouldReturnBadRequest() throws Exception {
        String testUsername = "testUsername";
        String testPassword = "testPassword";

        String requestBody = "{\"username\":\"" + testUsername + "\",\"password\":\""
                + testPassword + "\"}";

        given(this.loginService.login(anyString(), anyString()))
                .willThrow(Exception.class);

        this.mvc.perform(post("/rest/auth/v1/login").characterEncoding("utf-8")
                .contentType(MediaType.APPLICATION_JSON).content(requestBody).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }
}