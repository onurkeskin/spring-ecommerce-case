package com.ecommerce.authorizationservice.integration;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withBadRequest;

import com.ecommerce.authorizationservice.service.LoginService;
import com.ecommerce.authorizationservice.service.impl.LoginServiceImpl;

@RunWith(SpringRunner.class)
@RestClientTest({ LoginServiceImpl.class })
@AutoConfigureWebClient(registerRestTemplate = true)
@ActiveProfiles("test")
public class LoginServiceTest {

    @Value("${keycloak.resource}")
    private String clientId;

    @Value("${keycloak.credentials.secret}")
    private String clientSecret;

    @Value("${keycloak.realm}")
    private String realm;

    @Value("${keycloak.auth-server-url}")
    private String serverUrl;

    private MockRestServiceServer server;

    @Autowired
    private RestTemplate restTemplate;

    LoginServiceImpl loginServiceImpl;

    @Before
    public void before() {
        server = MockRestServiceServer.createServer(restTemplate);
        loginServiceImpl = new LoginServiceImpl(restTemplate, clientId, clientSecret, realm, serverUrl);
    }

    @Test
    public void LoginWithValidCredentialsShouldReturnToken() throws Exception {
        final String tokenServiceURI = String.format("%s/realms/%s/protocol/openid-connect/token", serverUrl, realm);
        final String testUser = "testUser";
        final String testPassword = "testPassword";
        final String testToken = "{\"token\": \"someToken\"}}";

        this.server.expect(requestTo(tokenServiceURI)).andRespond(withSuccess(testToken, MediaType.APPLICATION_JSON));

        ResponseEntity<String> result = loginServiceImpl.login(testUser, testPassword);

        assertThat(result.getBody()).isEqualTo(testToken);
    }

    @Test
    public void LoginWithInvalidCredentialsShouldReturnBad() throws Exception {
        final String tokenServiceURI = String.format("%s/realms/%s/protocol/openid-connect/token", serverUrl, realm);
        final String testUser = "testUser";
        final String testPassword = "testPassword";
        final String testToken = "{\"token\": \"someToken\"}}";

        this.server.expect(requestTo(tokenServiceURI)).andRespond(withBadRequest());

        assertThrows(Exception.class, () -> {
            ResponseEntity<String> result = loginServiceImpl.login(testUser, testPassword);
            assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        });

    }
}
