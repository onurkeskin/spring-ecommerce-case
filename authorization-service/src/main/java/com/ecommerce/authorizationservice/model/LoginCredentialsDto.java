package com.ecommerce.authorizationservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Dto for Login Credentials
 * 
 * @author Onur Keskin
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginCredentialsDto {
    private String username;
    private String password;
}
