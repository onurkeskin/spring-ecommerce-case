package com.ecommerce.authorizationservice.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import lombok.AllArgsConstructor;

import com.ecommerce.authorizationservice.model.LoginCredentialsDto;
import com.ecommerce.authorizationservice.service.LoginService;

@RestController
@RequestMapping("/rest/auth/v1")
@AllArgsConstructor
public class LoginController {

    private final LoginService loginService;

    @PostMapping(value = "login")
    public ResponseEntity<String> login(@RequestBody LoginCredentialsDto loginCred) {
        ResponseEntity<String> result = null;
        try {
            result = loginService.login(loginCred.getUsername(), loginCred.getPassword());
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }

        return result;
    }
}