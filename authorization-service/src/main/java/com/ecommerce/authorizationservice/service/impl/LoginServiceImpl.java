package com.ecommerce.authorizationservice.service.impl;

import java.util.HashMap;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.ecommerce.authorizationservice.service.LoginService;

@Service
public class LoginServiceImpl implements LoginService {

    private static final String grantTypeString = "grant_type";
    private static final String clientIdString = "client_id";
    private static final String clientSecretString = "client_secret";
    private static final String usernameString = "username";
    private static final String passwordString = "password";

    @Autowired
    private RestTemplate restTemplate;

    private final String clientId;

    private final String clientSecret;

    private final String realm;

    private final String serverUrl;

    public LoginServiceImpl(RestTemplate restTemplate, @Value("${keycloak.resource}") String clientId,
            @Value("${keycloak.credentials.secret}") String clientSecret, @Value("${keycloak.realm}") String realm,
            @Value("${keycloak.auth-server-url}") String serverUrl) {
        this.restTemplate = restTemplate;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.realm = realm;
        this.serverUrl = serverUrl;
    }

    @Override
    public ResponseEntity<String> login(String username, String password) throws Exception {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> form = new LinkedMultiValueMap<>();
        String grantType = "password";
        form.add(grantTypeString, grantType);
        form.add(clientIdString, clientId);
        form.add(clientSecretString, clientSecret);
        form.add(usernameString, username);
        form.add(passwordString, password);

        final String tokenServiceURI = String.format("%s/realms/%s/protocol/openid-connect/token", serverUrl, realm);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(form,
                headers);

        ResponseEntity<String> response = null;

        try {
            response = restTemplate.postForEntity(tokenServiceURI, request, String.class);
        } catch (Exception e) {
            throw new Exception("User Not Found");
        }

        return response;
    }

}
