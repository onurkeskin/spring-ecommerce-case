package com.ecommerce.authorizationservice.service;

import java.util.Map;

import org.springframework.http.ResponseEntity;

/**
 * Requests access token from keycloak
 * 
 * @author Onur Keskin
 */
public interface LoginService {
    ResponseEntity<String> login(String username, String password) throws Exception;
}

