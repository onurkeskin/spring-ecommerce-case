echo "Performing a clean Maven build"
./mvnw clean package -DskipTests=true

echo "Processing configuration-provider-service"
cd configuration-provider-service
docker build --tag configuration-provider-service .
cd ..

echo "Processing gateway-service"
cd gateway-service
docker build --tag gateway-service .
cd ..

echo "Processing user-service"
cd user-service
docker build --tag user-service .
cd ..

echo "Processing mail-service"
cd mail-service
docker build --tag mail-service .
cd ..

echo "Processing Building the shopping-service"
cd shopping-service
docker build --tag shopping-service .
cd ..