package com.ecommerce.mailservice.configuration;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;

import org.springframework.kafka.support.serializer.ErrorHandlingDeserializer2;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.KafkaTestUtils;

import com.ecommerce.mailservice.component.kafka.consumer.Receiver;
import com.ecommerce.mailservice.model.AccountCreationEvent;


/**
 * Configuration options for kafka.
 * 
 * @author Onur Keskin
 */
@Configuration
@EmbeddedKafka
@Profile("test")
public class ReceiverTestConfiguration {

    // @Value("${spring.kafka.bootstrap-servers}")
    // private String bootstrapServers;

    @Autowired
    private EmbeddedKafkaBroker embeddedKafkaBroker;

    @Value("${spring.kafka.consumer.group-id}")
    private String consumerGroupId;
    
    @Bean
    public Map<String, Object> consumerConfigs() {
        Map<String, Object> properties = KafkaTestUtils.consumerProps(consumerGroupId, "false", embeddedKafkaBroker);
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, consumerGroupId);
        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        
        return properties;
    }

    @Bean
    public ConsumerFactory<String, AccountCreationEvent> consumerFactory() {
        JsonDeserializer<AccountCreationEvent> jsonDeserializer = new JsonDeserializer<>(AccountCreationEvent.class, false);
        jsonDeserializer.addTrustedPackages("*");

        ErrorHandlingDeserializer2<AccountCreationEvent> errorHandlingDeserializer
        = new ErrorHandlingDeserializer2<>(jsonDeserializer);
        
        return new DefaultKafkaConsumerFactory<>(consumerConfigs(), new StringDeserializer(),
            errorHandlingDeserializer);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, AccountCreationEvent> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, AccountCreationEvent> kafkaListenerContainerFactory =
            new ConcurrentKafkaListenerContainerFactory<>();
                kafkaListenerContainerFactory.setConsumerFactory(consumerFactory());

        return kafkaListenerContainerFactory;
    }

    @Bean
    public Receiver receiver() {
        return new Receiver();
    }
}
