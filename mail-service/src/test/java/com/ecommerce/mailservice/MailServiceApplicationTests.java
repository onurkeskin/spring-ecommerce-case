package com.ecommerce.mailservice;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
@EmbeddedKafka(topics = { MailServiceApplicationTests.ACCOUNT_CREATION_TOPIC }, brokerProperties = {"log.dir=target/kafka"})
public class MailServiceApplicationTests {
    static final String ACCOUNT_CREATION_TOPIC = "ACCOUNT_CREATION_TOPIC";

	@Test
	void contextLoads() {
	}

}
