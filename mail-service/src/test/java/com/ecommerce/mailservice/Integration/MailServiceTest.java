package com.ecommerce.mailservice.Integration;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyObject;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.BDDMockito.given;

import java.time.Instant;
import java.util.UUID;

import com.ecommerce.mailservice.model.AccountCreationEvent;
import com.ecommerce.mailservice.model.RegistrationTokenDto;
import com.ecommerce.mailservice.service.MailService;
import com.ecommerce.mailservice.service.impl.MailServiceImpl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mail.MailAuthenticationException;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class MailServiceTest {
    @MockBean
    private JavaMailSender javaMailSender;

    private MailService mailService;

    @Before
    public void before() {
        mailService = new MailServiceImpl(javaMailSender);
    }

    @Test
    public void SendConfirmationMailShouldSendOnAuthenticated() throws Exception {

        String testUserID = UUID.randomUUID().toString();
        String testUserUsername = "test";
        String testUserEmail = "test@test.com";
        String testUserToken = UUID.randomUUID().toString();
        Long testTimestamp = Instant.now().getEpochSecond();
        AccountCreationEvent toSend = new AccountCreationEvent(testUserID, testUserUsername, testUserEmail,
                new RegistrationTokenDto(testUserToken, testTimestamp));

        mailService.sendConfirmationMail(toSend);
        verify(javaMailSender, times(1)).send(any(SimpleMailMessage.class));
    }

    @Test
    public void SendConfirmationMailShouldThrowOnNonAuthenticated() throws Exception {
        String testUserID = UUID.randomUUID().toString();
        String testUserUsername = "test";
        String testUserEmail = "test@test.com";
        String testUserToken = UUID.randomUUID().toString();
        Long testTimestamp = Instant.now().getEpochSecond();
        AccountCreationEvent toSend = new AccountCreationEvent(testUserID, testUserUsername, testUserEmail,
                new RegistrationTokenDto(testUserToken, testTimestamp));

        doThrow(MailAuthenticationException.class).when(javaMailSender).send(any(SimpleMailMessage.class));

        assertThrows(MailAuthenticationException.class, () -> {
            mailService.sendConfirmationMail(toSend);
        });
        verify(javaMailSender, times(1)).send(any(SimpleMailMessage.class));
    }
}
