package com.ecommerce.mailservice.Integration;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.ecommerce.mailservice.component.kafka.consumer.Receiver;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import com.ecommerce.mailservice.model.AccountCreationEvent;
import com.ecommerce.mailservice.model.RegistrationTokenDto;
import com.ecommerce.mailservice.service.impl.MailServiceImpl;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerConfig;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.ContainerTestUtils;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.mail.javamail.JavaMailSender;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.any;

@SpringBootTest
@DirtiesContext
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@EmbeddedKafka(partitions = 1, topics = { KafkaEventRecieveTest.ACCOUNT_CREATION_TOPIC })
public class KafkaEventRecieveTest {
    static final String ACCOUNT_CREATION_TOPIC = "ACCOUNT_CREATION_TOPIC";

    @Autowired
    private EmbeddedKafkaBroker embeddedKafkaBroker;
    
    @Autowired
    Receiver reciever;

    @MockBean
    private MailServiceImpl mailServiceImpl;

    private KafkaTemplate<String, AccountCreationEvent> sender;

    @Before
    public void before() {
        Map<String, Object> senderProps = KafkaTestUtils.producerProps(embeddedKafkaBroker);
        senderProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        senderProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);

        sender = new KafkaTemplate<String, AccountCreationEvent>(
                new DefaultKafkaProducerFactory<String, AccountCreationEvent>(senderProps));
    }

    @Test
    public void ShouldSendEmailOnEventRecieve() throws Exception {
        
        String testUserID = UUID.randomUUID().toString();
        String testUserUsername = "test";
        String testUserEmail = "test@test.com";
        String testUserToken = UUID.randomUUID().toString();
        Long testTimestamp = Instant.now().getEpochSecond();
        AccountCreationEvent toSend = new AccountCreationEvent(testUserID, testUserUsername, testUserEmail,
                new RegistrationTokenDto(testUserToken, testTimestamp));

        sender.send(ACCOUNT_CREATION_TOPIC, toSend);

        reciever.getLatch().await(10000, TimeUnit.MILLISECONDS);

        assertThat(reciever.getLatch().getCount()).isEqualTo(0l);
        assertThat(reciever.getRecieved()).usingRecursiveComparison().isEqualTo(toSend);
    }
}
