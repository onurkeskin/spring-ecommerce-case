package com.ecommerce.mailservice.unit;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.containsString;

import com.ecommerce.mailservice.model.AccountCreationEvent;
import com.ecommerce.mailservice.model.RegistrationTokenDto;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
public class AccountCreationEventTests {
    @Test
    public void AccountCreationEventNoArgConsturctibleToNullFieldsTest() throws Exception {
        assertDoesNotThrow(() -> {
            AccountCreationEvent dto = new AccountCreationEvent();
            assertThat(dto.getId()).isNull();
            assertThat(dto.getEmail()).isNull();
            assertThat(dto.getUsername()).isNull();
            assertThat(dto.getToken()).isNull();
        });
    }

    @Test
    public void AccountCreationEventAllArgsConsturctibleTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testId = "testId";
            String testEmail = "test@email.com";
            String testUsername = "testUser";
            RegistrationTokenDto testToken = new RegistrationTokenDto();

            AccountCreationEvent dto = new AccountCreationEvent(testId, testUsername, testEmail, testToken);
            assertThat(dto.getId()).isEqualTo(testId);
            assertThat(dto.getEmail()).isEqualTo(testEmail);
            assertThat(dto.getUsername()).isEqualTo(testUsername);
            assertThat(dto.getToken()).usingRecursiveComparison().isEqualTo(testToken);
        });
    }

    @Test
    public void GetIdTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testId = "testId";

            AccountCreationEvent dto = new AccountCreationEvent(testId, "testUsername", "testEmail",
                    new RegistrationTokenDto());
            assertThat(dto.getId()).isEqualTo(testId);
        });
    }

    @Test
    public void GetEmailTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testEmail = "test@email.com";

            AccountCreationEvent dto = new AccountCreationEvent("testId", "testUsername", testEmail,
                    new RegistrationTokenDto());
            assertThat(dto.getEmail()).isEqualTo(testEmail);
        });
    }

    @Test
    public void GetUsernameTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testUsername = "testUsername";

            AccountCreationEvent dto = new AccountCreationEvent("testId", testUsername, "testEmail",
                    new RegistrationTokenDto());
            assertThat(dto.getUsername()).isEqualTo(testUsername);
        });
    }

    @Test
    public void GetTokenTest() throws Exception {
        assertDoesNotThrow(() -> {
            RegistrationTokenDto testToken = new RegistrationTokenDto();

            AccountCreationEvent dto = new AccountCreationEvent("testId", "testUsername", "testEmail", testToken);
            assertThat(dto.getToken()).isEqualTo(testToken);
        });
    }

    @Test
    public void SetIdUpdatesIdTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testId = "testId";
            String testId2 = "testId2";

            AccountCreationEvent dto = new AccountCreationEvent(testId, "testUsername", "testEmail",
                    new RegistrationTokenDto());
            assertThat(dto.getId()).isEqualTo(testId);
            dto.setId(testId2);
            assertThat(dto.getId()).isEqualTo(testId2);
        });
    }

    @Test
    public void SetEmailUpdatesEmailTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testEmail = "test@email.com";
            String testEmail2 = "test2@email.com";

            AccountCreationEvent dto = new AccountCreationEvent("testId", "testUsername", testEmail,
                    new RegistrationTokenDto());
            assertThat(dto.getEmail()).isEqualTo(testEmail);
            dto.setEmail(testEmail2);
            assertThat(dto.getEmail()).isEqualTo(testEmail2);
        });
    }

    @Test
    public void SetUsernameUpdatesUsernameTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testUsername = "testUsername";
            String testUsername2 = "testUsername2";

            AccountCreationEvent dto = new AccountCreationEvent("testId", testUsername, "testEmail",
                    new RegistrationTokenDto());
            assertThat(dto.getUsername()).isEqualTo(testUsername);
            dto.setUsername(testUsername2);
            assertThat(dto.getUsername()).isEqualTo(testUsername2);
        });
    }

    @Test
    public void AccountReferenceEqualityTest() throws Exception {
        assertDoesNotThrow(() -> {
            AccountCreationEvent dto = new AccountCreationEvent("testId", "testUsername", "testEmail",
                    new RegistrationTokenDto());
            assertThat(dto.equals(dto)).isTrue();
        });
    }

    @Test
    public void AccountNullEqualityTest() throws Exception {
        assertDoesNotThrow(() -> {
            AccountCreationEvent dto = new AccountCreationEvent("testId", "testUsername", "testEmail",
                    new RegistrationTokenDto());
            assertThat(dto.equals(null)).isFalse();
        });
    }

    @Test
    public void AccountDeepEqualityTest() throws Exception {
        assertDoesNotThrow(() -> {
            AccountCreationEvent dto = new AccountCreationEvent("testId", "testUsername", "testEmail",
                    new RegistrationTokenDto());
            AccountCreationEvent dto2 = new AccountCreationEvent("testId", "testUsername", "testEmail",
                    new RegistrationTokenDto());
            AccountCreationEvent dto3 = new AccountCreationEvent("testId", "testEmail2", "testUsername",
                    new RegistrationTokenDto());
            assertThat(dto.equals(dto2)).isTrue();
            assertThat(dto.equals(dto3)).isFalse();
        });
    }

    @Test
    public void AccountHashCodeSameRefMustSameResultTest() throws Exception {
        assertDoesNotThrow(() -> {
            AccountCreationEvent dto = new AccountCreationEvent("testId", "testUsername", "testEmail",
                    new RegistrationTokenDto());
            assertThat(dto.hashCode()).isEqualTo(dto.hashCode());
        });
    }

    @Test
    public void AccountHashCodeDifferentRefMustSameFieldsMustBeSameTest() throws Exception {
        assertDoesNotThrow(() -> {
            AccountCreationEvent dto = new AccountCreationEvent("testId", "testUsername", "testEmail",
                    new RegistrationTokenDto());
            AccountCreationEvent dto2 = new AccountCreationEvent("testId", "testUsername", "testEmail",
                    new RegistrationTokenDto());
            assertThat(dto.hashCode()).isEqualTo(dto2.hashCode());
        });
    }

    @Test
    public void ToStringTest() throws Exception {
        assertDoesNotThrow(() -> {
            AccountCreationEvent dto = new AccountCreationEvent("testId", "testUsername", "testEmail",
                    new RegistrationTokenDto());
            assertThat(dto.toString(), CoreMatchers.allOf(containsString("testEmail"), containsString("testUsername"),
                    containsString("testId"),containsString("RegistrationTokenDto")));
        });

    }
}
