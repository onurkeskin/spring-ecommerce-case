package com.ecommerce.mailservice.unit;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.containsString;

import com.ecommerce.mailservice.model.RegistrationTokenDto;

import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
public class RegistrationTokenDtoTest {
    
    @Test
    public void RegistrationTokenDtoNoArgConsturctibleToNullFieldsTest() throws Exception {
        assertDoesNotThrow(() -> {
            RegistrationTokenDto dto = new RegistrationTokenDto();
            assertThat(dto.getRegistrationToken()).isNull();
            assertThat(dto.getCreatedDate()).isZero();
        });
    }

    @Test
    public void RegistrationTokenDtoAllArgsConsturctibleTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testRegistrationToken = "testToken";
            Long testCreatedDate = 0l;

            RegistrationTokenDto dto = new RegistrationTokenDto(testRegistrationToken, testCreatedDate);
            assertThat(dto.getRegistrationToken()).isEqualTo(testRegistrationToken);
            assertThat(dto.getCreatedDate()).isEqualTo(testCreatedDate);
        });
    }

    @Test
    public void GetRegistrationTokenTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testRegistrationToken = "testToken";

            RegistrationTokenDto dto = new RegistrationTokenDto(testRegistrationToken, 0l);
            assertThat(dto.getRegistrationToken()).isEqualTo(testRegistrationToken);
        });
    }

    @Test
    public void GetCreatedDateTest() throws Exception {
        assertDoesNotThrow(() -> {
            Long testCreatedDate = 0l;

            RegistrationTokenDto dto = new RegistrationTokenDto("testRegistrationToken", testCreatedDate);
            assertThat(dto.getCreatedDate()).isEqualTo(testCreatedDate);
        });
    }

    @Test
    public void SetRegistrationTokenUpdatesRegistrationTokenTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testRegistrationToken = "testToken";
            String testRegistrationToken2 = "testToken2";

            RegistrationTokenDto dto = new RegistrationTokenDto(testRegistrationToken, 0l);
            assertThat(dto.getRegistrationToken()).isEqualTo(testRegistrationToken);
            dto.setRegistrationToken(testRegistrationToken2);
            assertThat(dto.getRegistrationToken()).isEqualTo(testRegistrationToken2);
        });
    }

    @Test
    public void SetCreatedDateUpdatesCreatedDateTest() throws Exception {
        assertDoesNotThrow(() -> {
            Long testCreatedDate = 0l;
            Long testCreatedDate2 = 1l;

            RegistrationTokenDto dto = new RegistrationTokenDto("testRegistrationToken", testCreatedDate);
            assertThat(dto.getCreatedDate()).isEqualTo(testCreatedDate);
            dto.setCreatedDate(testCreatedDate2);
            assertThat(dto.getCreatedDate()).isEqualTo(testCreatedDate2);
        });
    }


    @Test
    public void RegistrationTokenDtoReferenceEqualityTest() throws Exception {
        assertDoesNotThrow(() -> {
            RegistrationTokenDto dto = new RegistrationTokenDto("testRegistrationToken", 0l);
            assertThat(dto.equals(dto)).isTrue();
        });
    }

    @Test
    public void RegistrationTokenDtoEqualityTest() throws Exception {
        assertDoesNotThrow(() -> {
            RegistrationTokenDto dto = new RegistrationTokenDto("testRegistrationToken", 0l);
            RegistrationTokenDto dto2 = new RegistrationTokenDto("testRegistrationToken", 0l);
            RegistrationTokenDto dto3 = new RegistrationTokenDto("testRegistrationToken2", 0l);
            assertThat(dto.equals(dto2)).isTrue();
            assertThat(dto.equals(dto3)).isFalse();
        });
    }

    @Test
    public void AccountHashCodeSameRefMustSameResultTest() throws Exception {
        assertDoesNotThrow(() -> {
            RegistrationTokenDto dto = new RegistrationTokenDto("testRegistrationToken", 0l);
            assertThat(dto.hashCode()).isEqualTo(dto.hashCode());
        });
    }

    @Test
    public void AccountHashCodeDifferentRefMustSameFieldsMustBeSameTest() throws Exception {
        assertDoesNotThrow(() -> {
            RegistrationTokenDto dto = new RegistrationTokenDto("testRegistrationToken", 0l);
            RegistrationTokenDto dto2 = new RegistrationTokenDto("testRegistrationToken", 0l);
            assertThat(dto.hashCode()).isEqualTo(dto2.hashCode());
        });
    }

    @Test
    public void ToStringTest() throws Exception {
        assertDoesNotThrow(() -> {
            RegistrationTokenDto dto = new RegistrationTokenDto("testRegistrationToken", 0l);
            assertThat(dto.toString(), CoreMatchers.allOf(containsString("testRegistrationToken"), containsString("0")));
        });
    }
}
