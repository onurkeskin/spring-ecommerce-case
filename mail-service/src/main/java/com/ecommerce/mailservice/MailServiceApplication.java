package com.ecommerce.mailservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


/**
 * Application that handles the email send requests for cloud components
 * 
 * @author Onur Keskin
 */
@EnableDiscoveryClient
@SpringBootApplication
public class MailServiceApplication {
	public static void main(String[] args) {
		SpringApplication.run(MailServiceApplication.class, args);
	}

}
