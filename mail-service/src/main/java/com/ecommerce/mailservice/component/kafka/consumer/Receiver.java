package com.ecommerce.mailservice.component.kafka.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;

import java.util.concurrent.CountDownLatch;

import com.ecommerce.mailservice.service.MailService;
import com.ecommerce.mailservice.model.AccountCreationEvent;

/**
 * Kafka event reciever, listening on kafka topics. Currently only listens on new users.
 * 
 * @author Onur Keskin
 */
public class Receiver {
    private CountDownLatch latch = new CountDownLatch(1);

    @Autowired 
    private MailService mailService;

    private AccountCreationEvent recieved = null;
    /**
     * @param account   Account object that requires email confirmation.
     */
    @KafkaListener(topics = "${spring.kafka.topic.accountCreated}")
    public void receive(AccountCreationEvent account) {
        recieved = account;
        mailService.sendConfirmationMail(account);
        latch.countDown();
    }

    public CountDownLatch getLatch() {
        return latch;
    }

    public AccountCreationEvent getRecieved() {
        return recieved;
    }
}
