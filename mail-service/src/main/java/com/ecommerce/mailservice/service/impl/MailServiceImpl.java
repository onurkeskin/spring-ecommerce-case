package com.ecommerce.mailservice.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

import com.ecommerce.mailservice.model.*;
import com.ecommerce.mailservice.service.MailService;


/**
 * Service implementation using JavaMailSender to handle emailing services.
 * 
 * @author Onur Keskin
 */
@Service
@RequiredArgsConstructor 
public class MailServiceImpl implements MailService {

    public final JavaMailSender javaMailSender;

    private Log log = LogFactory.getLog(getClass());

    /**
     * Sends User a confirmation email.
     * 
     * @param acc The account that is going to recieve confirmation email.
     */
    @Override
    public void sendConfirmationMail(AccountCreationEvent acc) {
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(acc.getEmail());
            message.setSubject("Account Confirmation Email");
            message.setText(String.format("Your User Id: %s\nYour Confirmation Token: %s", acc.getId() ,acc.getToken().getRegistrationToken()));

            javaMailSender.send(message);
        } catch (MailException exception) {
            log.info("Java mail sender exception : " + exception.toString());
            throw exception;
        }
    }

}
