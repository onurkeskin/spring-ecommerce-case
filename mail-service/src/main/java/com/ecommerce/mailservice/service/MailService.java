package com.ecommerce.mailservice.service;

import com.ecommerce.mailservice.model.*;

public interface MailService {
    void sendConfirmationMail(AccountCreationEvent acc);
}