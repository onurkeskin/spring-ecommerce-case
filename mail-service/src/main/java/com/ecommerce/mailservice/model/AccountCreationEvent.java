package com.ecommerce.mailservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * Dto that encapsulates the necessary information for sending confirmation token to the user.
 * 
 * @author Onur Keskin
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountCreationEvent {
    String id;
    String username;
    String email;
    RegistrationTokenDto token;
}
