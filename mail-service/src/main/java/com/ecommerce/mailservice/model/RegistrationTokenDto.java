package com.ecommerce.mailservice.model;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationTokenDto {
    private String registrationToken;

    private long createdDate;
}