package com.ecommerce.configurationproviderservice;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


/**
 * Handles the pod configurations in a cloud ecosystem. 
 * Currently test configurations are useless as cloud testing modules have not been implemented.
 * 
 * @author Onur Keskin
 */
@EnableDiscoveryClient
@EnableConfigServer
@SpringBootApplication
public class ConfigurationProviderServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfigurationProviderServiceApplication.class, args);
	}

}
