package com.ecommerce.userservice.mapper;

import com.ecommerce.userservice.domain.RegistrationToken;
import com.ecommerce.userservice.model.RegistrationTokenDto;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * Mapper class for converting Request/Response models to DTOs or vice versa.
 * 
 * @author Onur Keskin
 */
@Mapper
public interface RegistrationTokenMapper {
    RegistrationTokenMapper INSTANCE = Mappers.getMapper(RegistrationTokenMapper.class);

    RegistrationToken RegistrationTokenDtoToRegistrationToken(RegistrationTokenDto RegistrationTokenDto);
    RegistrationTokenDto RegistrationTokenToRegistrationTokenDto(RegistrationToken RegistrationTokenDto);
}
