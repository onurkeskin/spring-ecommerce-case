package com.ecommerce.userservice.service.impl;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.StatusType;

import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

import com.ecommerce.userservice.component.kafka.producer.Sender;
import com.ecommerce.userservice.dao.*;

import com.ecommerce.userservice.domain.RegistrationToken;
import com.ecommerce.userservice.mapper.RegistrationTokenMapper;
import com.ecommerce.userservice.model.AccountCreationEvent;
import com.ecommerce.userservice.model.AccountDto;
import com.ecommerce.userservice.model.exceptions.AccountAlreadyExistsException;

import com.ecommerce.userservice.service.AccountService;
import com.ecommerce.userservice.service.RegisterationTokenService;

/**
 * Service implementation for handling the Account related operations.
 * 
 * @author Onur Keskin
 */
@Service
@Slf4j
public class AccountServiceImpl implements AccountService {

    private final String ACCOUNT_CREATION_TOPIC;
    private final String realm;
    private final RegisterationTokenService registerationTokenService;
    private final Keycloak keycloakInstance;
    private final Sender sender;

    public AccountServiceImpl(
            @Value("${spring.kafka.topic.accountCreated}") String ACCOUNT_CREATION_TOPIC,
            @Value("${keycloak.realm}") String realm, 
            RegisterationTokenService registerationTokenService,
            Keycloak keycloakInstance, 
            Sender sender) {
        this.ACCOUNT_CREATION_TOPIC = ACCOUNT_CREATION_TOPIC;
        this.realm = realm;
        this.registerationTokenService = registerationTokenService;
        this.keycloakInstance = keycloakInstance;
        this.sender = sender;
    }

    /**
     * Account Registeration service. Streams account created event if account
     * succesfully created.
     * 
     * @param account to be registered.
     * @throws AccountAlreadyExistsException if email or username related to the
     *                                       account already exists.
     */
    @Override
    public UserRepresentation registerAccount(String email, String userName, String password)
            throws AccountAlreadyExistsException {
        UsersResource userResource = keycloakInstance.realm(realm).users();

        UserRepresentation userRepresentation = new UserRepresentation();
        userRepresentation.setEmail(email);
        userRepresentation.setUsername(userName);
        userRepresentation.setEnabled(false);

        CredentialRepresentation passwordCredential = new CredentialRepresentation();
        passwordCredential.setTemporary(false);
        passwordCredential.setType(CredentialRepresentation.PASSWORD);
        passwordCredential.setValue(password);

        userRepresentation.setCredentials(Arrays.asList(passwordCredential));

        Response resp = userResource.create(userRepresentation);
        if (resp.getStatusInfo().getStatusCode() != 200)
            throw new AccountAlreadyExistsException("Account with username or email already exists");

        List<UserRepresentation> expectSingleFrom = userResource.search(userName, true);
        UserRepresentation rep = expectSingleFrom.get(0);

        RegistrationToken registrationToken = new RegistrationToken();
        registrationToken.setAccountID(UUID.fromString(rep.getId()));
        registerationTokenService.createRegisterationToken(registrationToken);

        AccountCreationEvent creationEvent = new AccountCreationEvent(rep.getId(), rep.getUsername(), rep.getEmail(),
                RegistrationTokenMapper.INSTANCE.RegistrationTokenToRegistrationTokenDto(registrationToken));
        sender.send(ACCOUNT_CREATION_TOPIC, creationEvent);

        return rep;
    }

    @Override
    public boolean verifyEmail(String userId, String registrationToken) throws Exception {
        UsersResource userResource = keycloakInstance.realm(realm).users();
        List<UserRepresentation> expectSingleFrom = userResource.search("id:" + userId, 0, 1, true);

        if (expectSingleFrom.size() != 1) {
            throw new Exception("Something went really wrong");
        }
        UserRepresentation curUser = expectSingleFrom.get(0);
        curUser.setEnabled(true);
        curUser.setEmailVerified(true);
        userResource.get(userId).update(curUser);
        return true;
    }

    @Override
    public UserRepresentation getUserInformation(String userID) throws Exception {
        UsersResource userResource = keycloakInstance.realm(realm).users();
        List<UserRepresentation> expectSingleFrom = userResource.search("id:" + userID, 0, 1, true);

        if (expectSingleFrom.size() != 1) {
            throw new Exception("Something went really wrong");
        }

        return expectSingleFrom.get(0);
    }
    // /**
    // * A handler required for Spring Security authentication.
    // *
    // * @throws UsernameNotFoundException if username has not been found.
    // * @param username
    // * @return UserDetails object
    // */
    // // @Override
    // public UserDetails loadUserByUsername(String username) throws
    // UsernameNotFoundException {

    // User user = accountRepository.findByUsername(username).map(account -> {
    // boolean active = true;
    // return new User(account.getUsername(), account.getPassword(), active, active,
    // active, active,
    // AuthorityUtils.createAuthorityList("ROLE_ADMIN", "ROLE_USER"));
    // }).orElseThrow(() -> new UsernameNotFoundException(
    // String.format("user with name: %s not found. Exception thrown", username)));

    // return user;
    // }
}
