package com.ecommerce.userservice.service;

import com.ecommerce.userservice.domain.RegistrationToken;

/**
 * Interface definition for RegisterationTokenService
 * 
 * @author Onur Keskin
 */
public interface RegisterationTokenService {
    RegistrationToken createRegisterationToken(RegistrationToken registrationToken);
}
