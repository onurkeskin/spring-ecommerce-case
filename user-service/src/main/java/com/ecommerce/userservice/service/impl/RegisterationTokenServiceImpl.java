package com.ecommerce.userservice.service.impl;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

import com.ecommerce.userservice.dao.RegistrationTokenRepository;
import com.ecommerce.userservice.domain.RegistrationToken;
import com.ecommerce.userservice.service.RegisterationTokenService;
import com.ecommerce.userservice.service.impl.RegisterationTokenServiceImpl;


/**
 * Service implementation of registration token handling. 
 * Currently not used, will probably be required for regenerating 
 * confirmation token if first one is invalidated somehow.
 * 
 * @author Onur Keskin
 */
@Service
@AllArgsConstructor
public class RegisterationTokenServiceImpl implements RegisterationTokenService {
	private RegistrationTokenRepository registerationTokenRepository;

	public RegistrationToken createRegisterationToken(RegistrationToken registrationToken) {
		return registerationTokenRepository.save(registrationToken);
	}
	
}