package com.ecommerce.userservice.service;

import com.ecommerce.userservice.model.exceptions.AccountAlreadyExistsException;

import org.keycloak.representations.idm.UserRepresentation;

/**
 * Interface definition for UserService
 * 
 * @author Onur Keskin
 */
public interface AccountService {
    UserRepresentation registerAccount(String email, String userName, String password)
            throws AccountAlreadyExistsException;

    UserRepresentation getUserInformation(String userID) throws Exception;

    boolean verifyEmail(String userId, String registrationToken) throws Exception;
}
