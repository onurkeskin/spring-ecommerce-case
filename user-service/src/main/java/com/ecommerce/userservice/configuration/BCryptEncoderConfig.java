package com.ecommerce.userservice.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


/**
 * Provides a password encoder to be used for password encryption.
 * 
 * @author Onur Keskin
 */
@Configuration
public class BCryptEncoderConfig {

    /**
     * Generates an encoder that uses bcrypt to encode passwords.
     * 
     * @return BCryptPasswordEncoder 
     */
    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}