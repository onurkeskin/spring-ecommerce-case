// package com.ecommerce.userservice.configuration;

// import org.springframework.beans.factory.annotation.Value;
// import org.springframework.context.annotation.Bean;
// import org.springframework.context.annotation.Configuration;
// import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
// import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
// import org.springframework.session.web.context.AbstractHttpSessionApplicationInitializer;
// import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

// /**
//  * Configures the jsession token management properties.
//  * This Configuration lets the service use redis based storage for user session tokens.
//  * 
//  * @author Onur Keskin
//  */
// @Configuration
// @EnableRedisHttpSession
// public class SessionConfiguration extends AbstractHttpSessionApplicationInitializer {
    
//     /**
//      * Redis Host address as it is wired from the configuration server.
//      */
//     @Value("${spring.redis.host}")
//     private String redisHost;

//     /**
//      * Redis Port as it is wired from the configuration server.
//      */
//     @Value("${spring.redis.port}")
//     private Integer redisPort;
    
//     /**
//      * 
//      * @return Redis Connection pool. 
//      */
//     @Bean
//     public JedisConnectionFactory connectionFactory() {
//         RedisStandaloneConfiguration config = new RedisStandaloneConfiguration(redisHost, redisPort);
//         JedisConnectionFactory jedisConFactory = new JedisConnectionFactory(config);
//         return jedisConFactory;
//     }
// }