package com.ecommerce.userservice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import lombok.AllArgsConstructor;

import java.security.Principal;

import com.ecommerce.userservice.model.AccountDto;
import com.ecommerce.userservice.model.exceptions.*;
import com.ecommerce.userservice.service.AccountService;

/**
 * User Services. Currently only for account creation
 * 
 * @author Onur Keskin
 */
@RestController
@RequestMapping(value = "rest/user/v1", produces = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class AccountServiceController {

	private AccountService accountService;

	/**
	 * Endpoint that registers a user with specified username, email and password.
	 * 
	 * @return Http OK if the account does not already exist @ return Http Bad
	 * @return Request if account already exists.
	 */
	@PostMapping("/register")
	@ResponseBody
	public ResponseEntity<String> register(@RequestBody AccountDto accountDto) {
		try {
			UserRepresentation account = accountService.registerAccount(accountDto.getEmail(), accountDto.getUsername(),
					accountDto.getPassword());

			return ResponseEntity.ok().body("{\"message\":\"Account Created\"}");
		} catch (AccountAlreadyExistsException ex) {
			return ResponseEntity.badRequest().body("{\"message\":\"Account Creation Failed\"}");
		}
	}

	/**
	 * Endpoint that is used to verify the tokens sent to users to see if the
	 * provided email during registration is theirs.
	 * 
	 * @return Http OK if the token and account matches
	 * @return Http Bad Request  if token and account doesnt match
	 */
	@GetMapping("/account-activation")
	@ResponseBody
	public ResponseEntity<String> verifyEmail(@RequestParam(value = "user-id", required = true) String userId,
			@RequestParam(value = "key", required = true) String registrationToken) {
		try {
			if (accountService.verifyEmail(userId, registrationToken)) {
				return ResponseEntity.ok().body("{\"message\":\"Account unlocked\"}");
			} else {
				return ResponseEntity.badRequest().body("{\"message\":\"Account not found\"}");
			}
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body("{\"message\":\"Account not found\"}");
		}
	}

	/**
	 * Endpoint that shows the users account information and roles.
	 * 
	 * @return Http OK with account information in body if a user is authenticated
	 * @return Http Bad Request if user is not authenticated
	 */
	@GetMapping
	@ResponseBody
	public ResponseEntity<UserRepresentation> userInformation(Principal principal) {
		if (principal == null) {
			return ResponseEntity.badRequest().build();
		}
		try {
			return ResponseEntity.ok().body(accountService.getUserInformation(principal.getName()));
		} catch (Exception ex) {
			return ResponseEntity.badRequest().build();
		}
	}
}
