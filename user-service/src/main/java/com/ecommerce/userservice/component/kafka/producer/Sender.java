package com.ecommerce.userservice.component.kafka.producer;

import com.ecommerce.userservice.domain.RegistrationToken;
import com.ecommerce.userservice.model.AccountCreationEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;


/**
 * Kafka event sender, user service uses it to stream account creation event for interested services in the ecosystem.
 * 
 * @author Onur Keskin
 */
@AllArgsConstructor
@NoArgsConstructor
public class Sender {
    
    @Autowired
    private KafkaTemplate<String, AccountCreationEvent> kafkaTemplate;

    public void send(String topic, AccountCreationEvent payload) {
        kafkaTemplate.send(topic, payload);
    }
}
