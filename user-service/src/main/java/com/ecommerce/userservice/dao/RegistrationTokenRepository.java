package com.ecommerce.userservice.dao;

import com.ecommerce.userservice.domain.*;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for tokens
 * 
 * @author Onur Keskin
 */
@Repository
public interface RegistrationTokenRepository extends JpaRepository<RegistrationToken, Long> {

}