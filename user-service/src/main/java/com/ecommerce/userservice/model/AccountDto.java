package com.ecommerce.userservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * Dto that encapsulates the necessary information for Account registration request
 * 
 * @author Onur Keskin
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountDto {
    public String email;
    public String username;
    public String password;
}
