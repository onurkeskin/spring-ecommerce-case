package com.ecommerce.userservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountCreationEvent {
    String id;
    String username;
    String email;
    RegistrationTokenDto token;
}
