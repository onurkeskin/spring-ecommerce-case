package com.ecommerce.userservice.model.exceptions;

import org.springframework.security.core.AuthenticationException;

public class AccountAlreadyExistsException extends AuthenticationException {

    public AccountAlreadyExistsException(final String msg) {
        super(msg);
    }

}