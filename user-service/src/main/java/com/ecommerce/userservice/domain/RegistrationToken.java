package com.ecommerce.userservice.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import java.time.Instant;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Basic Registration Token Entity.
 * 
 * @author Onur Keskin
 */
@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
@Entity
public class RegistrationToken {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private UUID accountID;

    private String registrationToken;

    private long createdDate;

    public RegistrationToken() {
        this.createdDate = Instant.now().getEpochSecond();
        this.registrationToken = UUID.randomUUID().toString();
    }
}
