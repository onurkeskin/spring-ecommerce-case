package com.ecommerce.userservice.configuration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import org.keycloak.admin.client.Keycloak;
import org.mockito.Mockito;

@Configuration
@Profile("test")
public class KeycloakConfiguration  {

    @Bean
    Keycloak getKeycloakClient() {
        return Mockito.mock(Keycloak.class);
    }

}