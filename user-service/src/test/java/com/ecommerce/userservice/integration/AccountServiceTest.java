package com.ecommerce.userservice.integration;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.ecommerce.userservice.component.kafka.producer.Sender;
import com.ecommerce.userservice.model.exceptions.AccountAlreadyExistsException;
import com.ecommerce.userservice.service.RegisterationTokenService;
import com.ecommerce.userservice.service.impl.AccountServiceImpl;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import com.ecommerce.userservice.model.AccountCreationEvent;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerConfig;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.ContainerTestUtils;
import org.springframework.kafka.test.utils.KafkaTestUtils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.any;

@SpringBootTest
@DirtiesContext
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@EmbeddedKafka(topics = { AccountServiceTest.ACCOUNT_CREATION_TOPIC })
public class AccountServiceTest {
    static final String ACCOUNT_CREATION_TOPIC = "ACCOUNT_CREATION_TOPIC";

    @Autowired
    private EmbeddedKafkaBroker embeddedKafkaBroker;

    @Autowired
    private Keycloak keycloakInstance;

    @MockBean
    private RegisterationTokenService tokenService;

    private AccountServiceImpl accountServiceImpl;
    private KafkaMessageListenerContainer<String, AccountCreationEvent> listener;
    private BlockingQueue<ConsumerRecord<String, AccountCreationEvent>> events;

    @Before
    public void before() {
        Map<String, Object> senderProps = KafkaTestUtils.producerProps(embeddedKafkaBroker);
        senderProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        senderProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        KafkaTemplate<String, AccountCreationEvent> sender = new KafkaTemplate<String, AccountCreationEvent>(
                new DefaultKafkaProducerFactory<String, AccountCreationEvent>(senderProps));

        events = new LinkedBlockingQueue<ConsumerRecord<String, AccountCreationEvent>>();

        Map<String, Object> consumerProps = KafkaTestUtils.consumerProps("testGroup", "false", embeddedKafkaBroker);
        consumerProps.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        consumerProps.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);

        JsonDeserializer<AccountCreationEvent> jsonDeserializer = new JsonDeserializer<>(AccountCreationEvent.class,
                false);
        jsonDeserializer.addTrustedPackages("com.ecommerce.userservice.domain");

        DefaultKafkaConsumerFactory<String, AccountCreationEvent> consumerFactory = new DefaultKafkaConsumerFactory<String, AccountCreationEvent>(
                consumerProps, new StringDeserializer(), jsonDeserializer);

        listener = new KafkaMessageListenerContainer<String, AccountCreationEvent>(consumerFactory,
                new ContainerProperties(ACCOUNT_CREATION_TOPIC));

        listener.setupMessageListener(new MessageListener<String, AccountCreationEvent>() {
            @Override
            public void onMessage(ConsumerRecord<String, AccountCreationEvent> event) {
                events.add(event);
            }
        });

        // start the container and underlying message listener
        listener.start();

        accountServiceImpl = new AccountServiceImpl("ACCOUNT_CREATION_TOPIC", "test", tokenService, keycloakInstance, new Sender(sender));

        // wait until the container has the required number of assigned partitions
        ContainerTestUtils.waitForAssignment(listener, embeddedKafkaBroker.getPartitionsPerTopic());

    }

    @After
    public void after() {
        // detach listener
        listener.stop();
    }

    @Test
    public void RegisterAccountShouldRegisterUserOnNotExistingAccount() throws Exception {
        String email = "test@test.com";
        String username = "testuser";
        String password = "testpass";

        RealmResource mockRealmResource = mock(RealmResource.class);
        UsersResource mockUsersResource = mock(UsersResource.class);

        when(this.keycloakInstance.realm(any())).thenReturn(mockRealmResource);
        when(mockRealmResource.users()).thenReturn(mockUsersResource);
        given(mockUsersResource.create(any())).willReturn(Response.status(Status.OK).build());

        UserRepresentation userCreated = new UserRepresentation();
        userCreated.setId(UUID.randomUUID().toString());
        userCreated.setUsername(email);
        userCreated.setEmail(password);
        given(mockUsersResource.search(anyString(), anyBoolean())).willReturn(Arrays.asList(userCreated));

        UserRepresentation returnedAccountFromService = accountServiceImpl.registerAccount(email, username, password);

        assertThat(returnedAccountFromService.getId()).isEqualTo(userCreated.getId());
        assertThat(returnedAccountFromService.getUsername()).isEqualTo(userCreated.getUsername());
        assertThat(returnedAccountFromService.getEmail()).isEqualTo(userCreated.getEmail());

        // check that the message was received
        ConsumerRecord<String, AccountCreationEvent> received = events.poll(10, TimeUnit.SECONDS);

        assertThat(received.value().getId()).isEqualTo(userCreated.getId());
        assertThat(received.value().getUsername()).isEqualTo(userCreated.getUsername());
        assertThat(received.value().getEmail()).isEqualTo(userCreated.getEmail());
    }

    @Test
    public void RegisterAccountShouldThrowOnExistingAccount() throws Exception {
        String email = "test@test.com";
        String username = "testuser";
        String password = "testpass";

        RealmResource mockRealmResource = mock(RealmResource.class);
        UsersResource mockUsersResource = mock(UsersResource.class);

        given(this.keycloakInstance.realm(any())).willReturn(mockRealmResource);
        given(mockRealmResource.users()).willReturn(mockUsersResource);
        given(mockUsersResource.create(any())).willReturn(Response.status(Status.BAD_REQUEST).build());

        assertThrows(AccountAlreadyExistsException.class, () -> {
            this.accountServiceImpl.registerAccount(email, username, password);
        });
    }

    @Test
    public void VerifyEmailShouldReturnTrueForUnverifiedAndExistingAccount() throws Exception {
        String userId = UUID.randomUUID().toString();
        String token = "testToken";

        RealmResource mockRealmResource = mock(RealmResource.class);
        UsersResource mockUsersResource = mock(UsersResource.class);
        UserResource mockUserResource = mock(UserResource.class);

        given(this.keycloakInstance.realm(any())).willReturn(mockRealmResource);
        given(mockRealmResource.users()).willReturn(mockUsersResource);

        UserRepresentation userSearched = new UserRepresentation();
        userSearched.setId(UUID.randomUUID().toString());
        userSearched.setEnabled(false);
        userSearched.setEmailVerified(false);

        given(mockUsersResource.search(anyString(), anyInt(), anyInt(), anyBoolean()))
                .willReturn(Arrays.asList(userSearched));
        given(mockUsersResource.get(userId)).willReturn(mockUserResource);

        this.accountServiceImpl.verifyEmail(userId, token);

        assertThat(userSearched.isEnabled()).isTrue();
        assertThat(userSearched.isEmailVerified()).isTrue();

    }

    @Test
    public void VerifyEmailShouldThrowOnNonExistingAccount() throws Exception {
        String userId = UUID.randomUUID().toString();
        String token = "testToken";

        RealmResource mockRealmResource = mock(RealmResource.class);
        UsersResource mockUsersResource = mock(UsersResource.class);

        given(this.keycloakInstance.realm(any())).willReturn(mockRealmResource);
        given(mockRealmResource.users()).willReturn(mockUsersResource);

        given(mockUsersResource.search(anyString(), anyBoolean())).willReturn(Arrays.asList());

        assertThrows(Exception.class, () -> {
            this.accountServiceImpl.verifyEmail(userId, token);
        });
    }

    @Test
    public void GetUserInformationShouldReturnInfoOnAuthenticatedUser() throws Exception {
        String userId = UUID.randomUUID().toString();

        RealmResource mockRealmResource = mock(RealmResource.class);
        UsersResource mockUsersResource = mock(UsersResource.class);

        given(this.keycloakInstance.realm(any())).willReturn(mockRealmResource);
        given(mockRealmResource.users()).willReturn(mockUsersResource);

        UserRepresentation userSearched = new UserRepresentation();
        userSearched.setId(UUID.randomUUID().toString());
        userSearched.setEnabled(false);
        userSearched.setEmailVerified(false);

        given(mockUsersResource.search(anyString(), anyInt(), anyInt(), anyBoolean()))
                .willReturn(Arrays.asList(userSearched));

        assertThat(this.accountServiceImpl.getUserInformation(userId)).isEqualTo(userSearched);

    }
}
