package com.ecommerce.userservice.integration;

import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import com.ecommerce.userservice.component.kafka.producer.Sender;
import com.ecommerce.userservice.dao.RegistrationTokenRepository;
import com.ecommerce.userservice.domain.RegistrationToken;
import com.ecommerce.userservice.model.exceptions.AccountAlreadyExistsException;
import com.ecommerce.userservice.service.impl.RegisterationTokenServiceImpl;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.UserRepresentation;

import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.when;
import static org.mockito.Mockito.mock;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.any;


@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles("test")
public class RegistrationTokenServiceTest {

    @MockBean
    private RegistrationTokenRepository tokenService;

    private RegisterationTokenServiceImpl registerationTokenServiceImpl;

    @Before
    public void before() {
        registerationTokenServiceImpl = new RegisterationTokenServiceImpl(tokenService);
    }

    @Test
    public void CreateRegistrationTokenShouldCreateRegistrationToken() throws Exception {
        RegistrationToken toAdd = new RegistrationToken();
        toAdd.setAccountID(UUID.randomUUID());
        toAdd.setId(0l);

        when(this.tokenService.save(toAdd)).thenReturn(toAdd);

        RegistrationToken addedEntity = registerationTokenServiceImpl.createRegisterationToken(toAdd);

        assertThat(addedEntity).isEqualTo(toAdd);
    }

}
