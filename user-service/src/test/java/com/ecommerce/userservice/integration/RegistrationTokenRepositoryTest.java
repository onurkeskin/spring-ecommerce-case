package com.ecommerce.userservice.integration;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.hamcrest.collection.HasItemInArray;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;

import org.assertj.core.condition.AllOf;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

import com.ecommerce.userservice.dao.RegistrationTokenRepository;
import com.ecommerce.userservice.domain.RegistrationToken;


@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles("test")
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class RegistrationTokenRepositoryTest {

    @Autowired
    private RegistrationTokenRepository RegistrationTokenRepository; 

    @Autowired
    private TestEntityManager entityManager; 

    @After
    public void clear() {
        this.entityManager.clear();
    }

    @Test
    public void findByIdShouldReturnRegistrationToken() throws Exception {
        UUID accountId = UUID.randomUUID();
        RegistrationToken token = new RegistrationToken();
        token.setAccountID(accountId);

        RegistrationToken toCheck = this.entityManager.persist(token);

        Optional<RegistrationToken> against = this.RegistrationTokenRepository.findById(toCheck.getId());
        assertThat(against.isPresent()).isTrue();

        assertThat(against.get().getId()).isEqualTo(toCheck.getId());
        assertThat(against.get().getAccountID()).isEqualTo(toCheck.getAccountID());
        assertThat(against.get().getCreatedDate()).isEqualTo(toCheck.getCreatedDate());
        assertThat(against.get().getRegistrationToken()).isEqualTo(toCheck.getRegistrationToken());
    }

}