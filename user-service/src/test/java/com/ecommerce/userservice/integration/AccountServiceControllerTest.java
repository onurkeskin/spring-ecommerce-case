package com.ecommerce.userservice.integration;

import java.util.Collections;
import java.util.HashSet;
import java.util.UUID;
import java.security.Principal;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.keycloak.representations.idm.UserRepresentation;
import org.mockito.Mockito;
import org.springframework.core.io.ClassPathResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.ecommerce.userservice.controller.AccountServiceController;
import com.ecommerce.userservice.service.AccountService;
import com.ecommerce.userservice.service.impl.AccountServiceImpl;
import com.ecommerce.userservice.model.*;
import com.ecommerce.userservice.model.exceptions.AccountAlreadyExistsException;
import com.ecommerce.userservice.domain.*;

@RunWith(SpringRunner.class)
@WebMvcTest(AccountServiceController.class)
@AutoConfigureMockMvc(addFilters = false)
@ActiveProfiles("test")
public class AccountServiceControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AccountService accountService;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @Test
    public void registerOnNonExistingAccountShouldRegisterAndNoThrow() throws Exception {
        String requestBody = "{\"email\":\"" + "test@test.com" + "\",\"username\":\"" + "test" + "\",\"password\":\""
                + "test" + "\"}";

        given(this.accountService.registerAccount(anyString(), anyString(), anyString()))
                .willReturn(new UserRepresentation());

        this.mvc.perform(post("/rest/user/v1/register").characterEncoding("utf-8")
                .contentType(MediaType.APPLICATION_JSON).content(requestBody).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json("{\"message\":\"Account Created\"}"));
    }

    @Test
    public void registerOnExistingAccountShouldReturnBadRequest() throws Exception {
        String requestBody = "{\"email\":\"" + "test@test.com" + "\",\"username\":\"" + "test" + "\",\"password\":\""
                + "test" + "\"}";

        given(this.accountService.registerAccount(anyString(), anyString(), anyString()))
                .willThrow(AccountAlreadyExistsException.class);

        this.mvc.perform(post("/rest/user/v1/register").characterEncoding("utf-8")
                .contentType(MediaType.APPLICATION_JSON).content(requestBody).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andExpect(content().json("{\"message\":\"Account Creation Failed\"}"));
    }

    @Test
    public void verifyEmailOnExistingNonVerifiedShouldReturnOk() throws Exception {
        String testId = "test";
        String testToken = "test";

        given(this.accountService.verifyEmail(anyString(), anyString())).willReturn(true);

        this.mvc.perform(get("/rest/user/v1/account-activation" + "?user-id=" + testId + "&key=" + testToken)
                .characterEncoding("utf-8").contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json("{\"message\":\"Account unlocked\"}"));
    }

    @Test
    public void verifyEmailOnExistingVerifiedShouldReturnAccountNotFound() throws Exception {
        String testId = "test";
        String testToken = "test";

        given(this.accountService.verifyEmail(anyString(), anyString())).willThrow(Exception.class);

        this.mvc.perform(get("/rest/user/v1/account-activation" + "?user-id=" + testId + "&key=" + testToken)
                .characterEncoding("utf-8").contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andExpect(content().json("{\"message\":\"Account not found\"}"));
    }

    
    @Test
    public void getUserDetailsOnAuthenticatedUserShouldReturnDetails() throws Exception {
        UUID userId = UUID.randomUUID();
        Principal mockPrincipal = Mockito.mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn(userId.toString());

        given(this.accountService.getUserInformation(mockPrincipal.toString())).willReturn(new UserRepresentation());

        this.mvc.perform(get("/rest/user/v1").principal(mockPrincipal)
                .characterEncoding("utf-8").contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getUserDetailsOnNonAuthenticatedUserShouldReturnBadRequest() throws Exception {
        this.mvc.perform(get("/rest/user/v1")
                .characterEncoding("utf-8").contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

}
