package com.ecommerce.userservice;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@SpringBootTest
@EmbeddedKafka(topics = { UserServiceApplicationTests.ACCOUNT_CREATION_TOPIC }, brokerProperties = {"log.dir=target/kafka"})
class UserServiceApplicationTests {
    static final String ACCOUNT_CREATION_TOPIC = "ACCOUNT_CREATION_TOPIC";

	@Test
	void contextLoads() {
	}

}
