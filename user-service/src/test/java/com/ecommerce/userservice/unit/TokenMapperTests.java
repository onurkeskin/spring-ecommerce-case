package com.ecommerce.userservice.unit;

import org.joda.time.Instant;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.UUID;

import com.ecommerce.userservice.domain.RegistrationToken;
import com.ecommerce.userservice.mapper.RegistrationTokenMapper;
import com.ecommerce.userservice.model.RegistrationTokenDto;

@ActiveProfiles("test")
public class TokenMapperTests {
    private RegistrationTokenMapper mapper;

    @Before
    public void setUp() throws Exception {
        this.mapper = RegistrationTokenMapper.INSTANCE;
    }

    @Test
    public void EntityToDtoNullTest() throws Exception {
        UUID id = UUID.randomUUID();
        
        RegistrationToken entity = null;
        RegistrationTokenDto testRegistrationToken = mapper.RegistrationTokenToRegistrationTokenDto(entity);
        
        assertThat(testRegistrationToken).isNull();
    }

    @Test
    public void EntityToDtoTest() throws Exception {
        UUID id = UUID.randomUUID();
        
        RegistrationToken entity = new RegistrationToken();
        entity.setId(0l);
        entity.setAccountID(UUID.randomUUID());
        RegistrationTokenDto testRegistrationToken = mapper.RegistrationTokenToRegistrationTokenDto(entity);
        
        assertThat(testRegistrationToken.getRegistrationToken()).isEqualTo(entity.getRegistrationToken());
        assertThat(testRegistrationToken.getCreatedDate()).isEqualTo(entity.getCreatedDate());
    }

    @Test
    public void DtoToEntityNullTest() throws Exception {
        UUID id = UUID.randomUUID();
        
        RegistrationTokenDto dto = null;
        RegistrationToken testRegistrationToken = mapper.RegistrationTokenDtoToRegistrationToken(dto);
        
        assertThat(testRegistrationToken).isNull();
    }

    @Test
    public void DtoToEntityTest() throws Exception {
        UUID id = UUID.randomUUID();
        
        RegistrationTokenDto dto = new RegistrationTokenDto();
        dto.setRegistrationToken(UUID.randomUUID().toString());
        dto.setCreatedDate(Instant.now().getMillis());
        RegistrationToken testRegistrationToken = mapper.RegistrationTokenDtoToRegistrationToken(dto);
        
        assertThat(testRegistrationToken.getRegistrationToken()).isEqualTo(dto.getRegistrationToken());
        assertThat(testRegistrationToken.getCreatedDate()).isEqualTo(dto.getCreatedDate());
    }
}