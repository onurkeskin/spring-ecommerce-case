package com.ecommerce.userservice.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.UUID;

import com.ecommerce.userservice.model.AccountDto;

@RunWith(SpringRunner.class)
@JsonTest
@ActiveProfiles("test")
public class AccountDtoJsonTest {
    @Autowired
    private JacksonTester<AccountDto> json;

    @Test
    public void deserializeJson() throws Exception {
        AccountDto dto = new AccountDto("testEmail", "testUsername", "testPassword");

        String content = "{\"email\": \"testEmail\", \"username\": \"testUsername\", \"password\": \"testPassword\"}";

        assertThat(this.json.parse(content)).isEqualTo(dto);
    }
}