package com.ecommerce.userservice.unit;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Instant;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.containsString;

import com.ecommerce.userservice.domain.RegistrationToken;

import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
public class RegistrationTokenTest {

    @Test
    public void RegistrationTokenNoArgConsturctibleToNullFieldsTest() throws Exception {

        assertDoesNotThrow(() -> {
            RegistrationToken token = new RegistrationToken();
            assertThat(token.getRegistrationToken()).isNotEmpty();
            assertThat(token.getCreatedDate()).isGreaterThan(0);
            assertThat(token.getId()).isNull();
            assertThat(token.getAccountID()).isNull();
        });

    }

    @Test
    public void RegistrationTokenAllArgsConsturctibleTest() throws Exception {
        assertDoesNotThrow(() -> {
            Long testId = 0l;
            UUID testAccountID = UUID.randomUUID();
            String testRegistrationToken = UUID.randomUUID().toString();
            long testCreatedDate = Instant.now().getEpochSecond();

            RegistrationToken token = new RegistrationToken(testId, testAccountID, testRegistrationToken,
                    testCreatedDate);
            assertThat(token.getId()).isEqualTo(testId);
            assertThat(token.getAccountID()).isEqualTo(testAccountID);
            assertThat(token.getRegistrationToken()).isEqualTo(testRegistrationToken);
            assertThat(token.getCreatedDate()).isEqualTo(testCreatedDate);
        });
    }

    @Test
    public void GetIdTest() throws Exception {
        assertDoesNotThrow(() -> {
            Long testId = 0l;
            UUID testAccountID = UUID.randomUUID();
            String testRegistrationToken = UUID.randomUUID().toString();
            long testCreatedDate = Instant.now().getEpochSecond();

            RegistrationToken token = new RegistrationToken(testId, testAccountID, testRegistrationToken,
                    testCreatedDate);
            assertThat(token.getId()).isEqualTo(testId);
        });
    }

    @Test
    public void GetUserIdTest() throws Exception {
        assertDoesNotThrow(() -> {
            Long testId = 0l;
            UUID testAccountID = UUID.randomUUID();
            String testRegistrationToken = UUID.randomUUID().toString();
            long testCreatedDate = Instant.now().getEpochSecond();

            RegistrationToken token = new RegistrationToken(testId, testAccountID, testRegistrationToken,
                    testCreatedDate);
            assertThat(token.getAccountID()).isEqualTo(testAccountID);
        });
    }

    @Test
    public void GetRegistrationTokenTest() throws Exception {
        assertDoesNotThrow(() -> {
            Long testId = 0l;
            UUID testAccountID = UUID.randomUUID();
            String testRegistrationToken = UUID.randomUUID().toString();
            long testCreatedDate = Instant.now().getEpochSecond();

            RegistrationToken token = new RegistrationToken(testId, testAccountID, testRegistrationToken,
                    testCreatedDate);
            assertThat(token.getRegistrationToken()).isEqualTo(testRegistrationToken);
        });
    }

    @Test
    public void GetCreatedDateTest() throws Exception {
        assertDoesNotThrow(() -> {
            Long testId = 0l;
            UUID testAccountID = UUID.randomUUID();
            String testRegistrationToken = UUID.randomUUID().toString();
            long testCreatedDate = Instant.now().getEpochSecond();

            RegistrationToken token = new RegistrationToken(testId, testAccountID, testRegistrationToken,
                    testCreatedDate);
            assertThat(token.getCreatedDate()).isEqualTo(testCreatedDate);
        });
    }

    @Test
    public void SetIdUpdatesIdTest() throws Exception {
        assertDoesNotThrow(() -> {
            Long testId = 0l;
            Long testId2 = 1l;
            UUID testAccountID = UUID.randomUUID();
            String testRegistrationToken = UUID.randomUUID().toString();
            long testCreatedDate = Instant.now().getEpochSecond();

            RegistrationToken token = new RegistrationToken(testId, testAccountID, testRegistrationToken,
                    testCreatedDate);
            assertThat(token.getId()).isEqualTo(testId);
            token.setId(testId2);
            assertThat(token.getId()).isEqualTo(testId2);
        });
    }

    @Test
    public void SetAccountIdUpdatesAccountIdTest() throws Exception {
        assertDoesNotThrow(() -> {
            Long testId = 0l;
            UUID testAccountID = UUID.randomUUID();
            UUID testAccountID2 = UUID.randomUUID();
            String testRegistrationToken = UUID.randomUUID().toString();
            long testCreatedDate = Instant.now().getEpochSecond();

            RegistrationToken token = new RegistrationToken(testId, testAccountID, testRegistrationToken,
                    testCreatedDate);
            assertThat(token.getAccountID()).isEqualTo(testAccountID);
            token.setAccountID(testAccountID2);
            assertThat(token.getAccountID()).isEqualTo(testAccountID2);
        });
    }

    @Test
    public void SetRegistrationTokenUpdatesRegistrationTokenTest() throws Exception {
        assertDoesNotThrow(() -> {
            Long testId = 0l;
            UUID testAccountID = UUID.randomUUID();
            String testRegistrationToken = UUID.randomUUID().toString();
            String testRegistrationToken2 = UUID.randomUUID().toString();
            long testCreatedDate = Instant.now().getEpochSecond();

            RegistrationToken token = new RegistrationToken(testId, testAccountID, testRegistrationToken,
                    testCreatedDate);
            assertThat(token.getRegistrationToken()).isEqualTo(testRegistrationToken);
            token.setRegistrationToken(testRegistrationToken2);
            assertThat(token.getRegistrationToken()).isEqualTo(testRegistrationToken2);
        });
    }

    @Test
    public void SetCreatedDateUpdatesCreatedDateTest() throws Exception {
        assertDoesNotThrow(() -> {
            Long testId = 0l;
            UUID testAccountID = UUID.randomUUID();
            String testRegistrationToken = UUID.randomUUID().toString();
            long testCreatedDate = Instant.now().getEpochSecond();
            long testCreatedDate2 = testCreatedDate + 2;

            RegistrationToken token = new RegistrationToken(testId, testAccountID, testRegistrationToken,
                    testCreatedDate);
            assertThat(token.getCreatedDate()).isEqualTo(testCreatedDate);
            token.setCreatedDate(testCreatedDate2);
            assertThat(token.getCreatedDate()).isEqualTo(testCreatedDate2);
        });
    }

    @Test
    public void RegistrationTokenReferenceEqualityTest() throws Exception {
        assertDoesNotThrow(() -> {
            Long testId = 0l;
            UUID testAccountID = UUID.randomUUID();
            String testRegistrationToken = UUID.randomUUID().toString();
            long testCreatedDate = Instant.now().getEpochSecond();
            long testCreatedDate2 = testCreatedDate + 2;

            RegistrationToken token = new RegistrationToken(testId, testAccountID, testRegistrationToken,
                    testCreatedDate);
            assertThat(token.equals(token)).isTrue();
        });
    }

    @Test
    public void RegistrationTokenEqualityTest() throws Exception {
        assertDoesNotThrow(() -> {
            Long testId = 0l;
            UUID testAccountID = UUID.randomUUID();
            String testRegistrationToken = UUID.randomUUID().toString();
            long testCreatedDate = Instant.now().getEpochSecond();
            long testCreatedDate2 = testCreatedDate + 2;

            RegistrationToken token = new RegistrationToken(testId, testAccountID, testRegistrationToken,
                    testCreatedDate);
            RegistrationToken token2 = new RegistrationToken(testId, testAccountID, testRegistrationToken,
                    testCreatedDate);
            RegistrationToken token3 = new RegistrationToken(testId, testAccountID, testRegistrationToken,
                    testCreatedDate2);
            assertThat(token.equals(token2)).isTrue();
            assertThat(token.equals(token3)).isFalse();
        });
    }

    @Test
    public void AccountHashCodeSameRefMustSameResultTest() throws Exception {
        Long testId = 0l;
        UUID testAccountID = UUID.randomUUID();
        String testRegistrationToken = UUID.randomUUID().toString();
        long testCreatedDate = Instant.now().getEpochSecond();

        assertDoesNotThrow(() -> {
            RegistrationToken token = new RegistrationToken(testId, testAccountID, testRegistrationToken,
                    testCreatedDate);
            assertThat(token.hashCode()).isEqualTo(token.hashCode());
        });
    }

    @Test
    public void AccountHashCodeDifferentRefMustSameFieldsMustBeSameTest() throws Exception {
        Long testId = 0l;
        UUID testAccountID = UUID.randomUUID();
        String testRegistrationToken = UUID.randomUUID().toString();
        long testCreatedDate = Instant.now().getEpochSecond();

        assertDoesNotThrow(() -> {
            RegistrationToken token = new RegistrationToken(testId, testAccountID, testRegistrationToken,
                    testCreatedDate);
            RegistrationToken token2 = new RegistrationToken(testId, testAccountID, testRegistrationToken,
                    testCreatedDate);
            assertThat(token.hashCode()).isEqualTo(token2.hashCode());
        });
    }
}
