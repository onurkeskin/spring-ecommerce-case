package com.ecommerce.userservice.unit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.UUID;

import com.ecommerce.userservice.model.RegistrationTokenDto;

@RunWith(SpringRunner.class)
@JsonTest
@ActiveProfiles("test")
public class RegistrationTokenDtoJsonTest {
    @Autowired
    private JacksonTester<RegistrationTokenDto> json;

    @Test
    public void deserializeJson() throws Exception {
        RegistrationTokenDto dto = new RegistrationTokenDto("testRegistrationToken", 0l);

        String content = "{\"registrationToken\": \"testRegistrationToken\", \"createdDate\": 0}";

        assertThat(this.json.parse(content)).isEqualTo(dto);
    }
}