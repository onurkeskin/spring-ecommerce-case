package com.ecommerce.userservice.unit;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.containsString;

import com.ecommerce.userservice.model.AccountDto;

import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
public class AccountDtoTest {

    @Test
    public void AccountDtoNoArgConsturctibleToNullFieldsTest() throws Exception {
        assertDoesNotThrow(() -> {
            AccountDto dto = new AccountDto();
            assertThat(dto.getEmail()).isNull();
            assertThat(dto.getUsername()).isNull();
            assertThat(dto.getPassword()).isNull();
        });
    }

    @Test
    public void AccountDtoAllArgsConsturctibleTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testEmail = "test@email.com";
            String testUsername = "testUser";
            String testPassword = "testPassword";

            AccountDto dto = new AccountDto(testEmail, testUsername, testPassword);
            assertThat(dto.getEmail()).isEqualTo(testEmail);
            assertThat(dto.getUsername()).isEqualTo(testUsername);
            assertThat(dto.getPassword()).isEqualTo(testPassword);
        });
    }

    @Test
    public void GetEmailTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testEmail = "test@email.com";

            AccountDto dto = new AccountDto(testEmail, "testUsername", "testPassword");
            assertThat(dto.getEmail()).isEqualTo(testEmail);
        });
    }

    @Test
    public void GetUsernameTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testUsername = "testUsername";

            AccountDto dto = new AccountDto("testEmail", testUsername, "testPassword");
            assertThat(dto.getUsername()).isEqualTo(testUsername);
        });
    }

    @Test
    public void GetPasswordTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testPassword = "testPassword";

            AccountDto dto = new AccountDto("testEmail", "testUsername", testPassword);
            assertThat(dto.getPassword()).isEqualTo(testPassword);
        });
    }

    @Test
    public void SetEmailUpdatesEmailTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testEmail = "test@email.com";
            String testEmail2 = "test2@email.com";

            AccountDto dto = new AccountDto(testEmail, "testUsername", "testPassword");
            assertThat(dto.getEmail()).isEqualTo(testEmail);
            dto.setEmail(testEmail2);
            assertThat(dto.getEmail()).isEqualTo(testEmail2);
        });
    }

    @Test
    public void SetUsernameUpdatesUsernameTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testUsername = "testUsername";
            String testUsername2 = "testUsername2";

            AccountDto dto = new AccountDto("testEmail", testUsername, "testPassword");
            assertThat(dto.getUsername()).isEqualTo(testUsername);
            dto.setUsername(testUsername2);
            assertThat(dto.getUsername()).isEqualTo(testUsername2);
        });
    }

    @Test
    public void SetPasswordUpdatesPasswordTest() throws Exception {
        assertDoesNotThrow(() -> {
            String testPassword = "testPassword";
            String testPassword2 = "testPassword2";

            AccountDto dto = new AccountDto("testEmail", "testUsername", testPassword);
            assertThat(dto.getPassword()).isEqualTo(testPassword);
            dto.setPassword(testPassword2);
            assertThat(dto.getPassword()).isEqualTo(testPassword2);
        });
    }

    @Test
    public void AccountReferenceEqualityTest() throws Exception {
        assertDoesNotThrow(() -> {
            AccountDto dto = new AccountDto("testEmail", "testUsername", "testPassword");
            assertThat(dto.equals(dto)).isTrue();
        });
    }

    @Test
    public void AccountDeepEqualityTest() throws Exception {
        assertDoesNotThrow(() -> {
            AccountDto dto = new AccountDto("testEmail", "testUsername", "testPassword");
            AccountDto dto2 = new AccountDto("testEmail", "testUsername", "testPassword");
            AccountDto dto3 = new AccountDto("testEmail2", "testUsername", "testPassword");
            assertThat(dto.equals(dto2)).isTrue();
            assertThat(dto.equals(dto3)).isFalse();
        });
    }

    @Test
    public void AccountHashCodeSameRefMustSameResultTest() throws Exception {
        assertDoesNotThrow(() -> {
            AccountDto dto = new AccountDto("testEmail", "testUsername", "testPassword");
            assertThat(dto.hashCode()).isEqualTo(dto.hashCode());
        });
    }

    @Test
    public void AccountHashCodeDifferentRefMustSameFieldsMustBeSameTest() throws Exception {
        assertDoesNotThrow(() -> {
            AccountDto dto = new AccountDto("testEmail", "testUsername", "testPassword");
            AccountDto dto2 = new AccountDto("testEmail", "testUsername", "testPassword");
            assertThat(dto.hashCode()).isEqualTo(dto2.hashCode());
        });
    }

    @Test
    public void ToStringTest() throws Exception {
        assertDoesNotThrow(() -> {
            AccountDto dto = new AccountDto("testEmail", "testUsername", "testPassword");
            assertThat(dto.toString(), CoreMatchers.allOf(containsString("testEmail"), containsString("testUsername"),
                    containsString("testPassword")));
        });
    }
}
