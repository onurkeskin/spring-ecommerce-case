![pipeline](https://gitlab.com/onurkeskin/spring-ecommerce-case/badges/master/pipeline.svg)
![coverage](https://gitlab.com/onurkeskin/spring-ecommerce-case/badges/master/coverage.svg)

# spring-ecommerce-case

A minimal ecommerce site with some microservice dust. 

# Dev Environment

You can use vscode dev container extension for all the build tools and environment i run my code in.

# Local Setup

run `./build.sh` for generating the containers.
use `docker-compose up` for creating a local service ecosystem.
However due to number of containers it needs, it requires 5-6 gigs of free memory.

# Architecture

- This project uses zuul as a gateway and reverse proxy.(Although again i backdoored some rest end points for convenience...)
- Uses consul for service discovery and configuration.
- Uses kafka for streaming account creation messages.
- Uses postgresql as a persistent database. (although may use in memory db if containers are start with -Dprofile flags some test profiles use h2 as dataSource)